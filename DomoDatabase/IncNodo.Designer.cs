﻿namespace DomoDatabase
{
    partial class IncNodo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.visperiodoacq = new System.Windows.Forms.TextBox();
            this.BtnIncNodo = new System.Windows.Forms.Button();
            this.nodoacq = new System.Windows.Forms.ComboBox();
            this.Selectnodoinc = new System.Windows.Forms.Label();
            this.btnindietro2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // visperiodoacq
            // 
            this.visperiodoacq.Location = new System.Drawing.Point(12, 12);
            this.visperiodoacq.Multiline = true;
            this.visperiodoacq.Name = "visperiodoacq";
            this.visperiodoacq.ReadOnly = true;
            this.visperiodoacq.Size = new System.Drawing.Size(426, 28);
            this.visperiodoacq.TabIndex = 29;
            // 
            // BtnIncNodo
            // 
            this.BtnIncNodo.Location = new System.Drawing.Point(363, 59);
            this.BtnIncNodo.Name = "BtnIncNodo";
            this.BtnIncNodo.Size = new System.Drawing.Size(75, 23);
            this.BtnIncNodo.TabIndex = 28;
            this.BtnIncNodo.Text = "Avanti";
            this.BtnIncNodo.UseVisualStyleBackColor = true;
            this.BtnIncNodo.Click += new System.EventHandler(this.BtnIncNodo_Click);
            // 
            // nodoacq
            // 
            this.nodoacq.FormattingEnabled = true;
            this.nodoacq.Location = new System.Drawing.Point(268, 59);
            this.nodoacq.Name = "nodoacq";
            this.nodoacq.Size = new System.Drawing.Size(89, 21);
            this.nodoacq.TabIndex = 27;
            // 
            // Selectnodoinc
            // 
            this.Selectnodoinc.AutoSize = true;
            this.Selectnodoinc.Location = new System.Drawing.Point(93, 64);
            this.Selectnodoinc.Name = "Selectnodoinc";
            this.Selectnodoinc.Size = new System.Drawing.Size(169, 13);
            this.Selectnodoinc.TabIndex = 26;
            this.Selectnodoinc.Text = "Seleziona il nodo da incrementare:";
            // 
            // btnindietro2
            // 
            this.btnindietro2.Location = new System.Drawing.Point(12, 57);
            this.btnindietro2.Name = "btnindietro2";
            this.btnindietro2.Size = new System.Drawing.Size(75, 23);
            this.btnindietro2.TabIndex = 30;
            this.btnindietro2.Text = "Indietro";
            this.btnindietro2.UseVisualStyleBackColor = true;
            this.btnindietro2.Click += new System.EventHandler(this.btnindietro2_Click);
            // 
            // IncNodo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 90);
            this.Controls.Add(this.btnindietro2);
            this.Controls.Add(this.visperiodoacq);
            this.Controls.Add(this.BtnIncNodo);
            this.Controls.Add(this.nodoacq);
            this.Controls.Add(this.Selectnodoinc);
            this.Name = "IncNodo";
            this.Text = "IncNodo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IncNodo_FormClosed);
            this.Load += new System.EventHandler(this.IncNodo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox visperiodoacq;
        private System.Windows.Forms.Button BtnIncNodo;
        public System.Windows.Forms.ComboBox nodoacq;
        private System.Windows.Forms.Label Selectnodoinc;
        private System.Windows.Forms.Button btnindietro2;
    }
}