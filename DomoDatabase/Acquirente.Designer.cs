﻿namespace DomoDatabase
{
    partial class domodatabaseacquirenti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtLongitudine = new System.Windows.Forms.TextBox();
            this.barra = new System.Windows.Forms.Label();
            this.TxtLatitudine = new System.Windows.Forms.TextBox();
            this.posizionegpscomp = new System.Windows.Forms.Label();
            this.TxtCitta = new System.Windows.Forms.TextBox();
            this.cittacomp = new System.Windows.Forms.Label();
            this.TxtProvincia = new System.Windows.Forms.TextBox();
            this.provinciacomp = new System.Windows.Forms.Label();
            this.TxtCap = new System.Windows.Forms.TextBox();
            this.capcomp = new System.Windows.Forms.Label();
            this.TxtIndirizzo = new System.Windows.Forms.TextBox();
            this.indirizzocomp = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.nomecomp = new System.Windows.Forms.Label();
            this.datiacq = new System.Windows.Forms.Label();
            this.btncinferma2 = new System.Windows.Forms.Button();
            this.bntannulla = new System.Windows.Forms.Button();
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.SuspendLayout();
            // 
            // TxtLongitudine
            // 
            this.TxtLongitudine.Location = new System.Drawing.Point(269, 236);
            this.TxtLongitudine.Margin = new System.Windows.Forms.Padding(4);
            this.TxtLongitudine.Name = "TxtLongitudine";
            this.TxtLongitudine.Size = new System.Drawing.Size(84, 24);
            this.TxtLongitudine.TabIndex = 46;
            // 
            // barra
            // 
            this.barra.AutoSize = true;
            this.barra.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barra.Location = new System.Drawing.Point(246, 236);
            this.barra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.barra.Name = "barra";
            this.barra.Size = new System.Drawing.Size(15, 24);
            this.barra.TabIndex = 45;
            this.barra.Text = "/";
            // 
            // TxtLatitudine
            // 
            this.TxtLatitudine.Location = new System.Drawing.Point(154, 236);
            this.TxtLatitudine.Margin = new System.Windows.Forms.Padding(4);
            this.TxtLatitudine.Name = "TxtLatitudine";
            this.TxtLatitudine.Size = new System.Drawing.Size(84, 24);
            this.TxtLatitudine.TabIndex = 44;
            // 
            // posizionegpscomp
            // 
            this.posizionegpscomp.AutoSize = true;
            this.posizionegpscomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posizionegpscomp.Location = new System.Drawing.Point(32, 239);
            this.posizionegpscomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.posizionegpscomp.Name = "posizionegpscomp";
            this.posizionegpscomp.Size = new System.Drawing.Size(114, 18);
            this.posizionegpscomp.TabIndex = 43;
            this.posizionegpscomp.Text = "Posizione GPS:";
            // 
            // TxtCitta
            // 
            this.TxtCitta.Location = new System.Drawing.Point(100, 157);
            this.TxtCitta.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCitta.Name = "TxtCitta";
            this.TxtCitta.Size = new System.Drawing.Size(253, 24);
            this.TxtCitta.TabIndex = 42;
            // 
            // cittacomp
            // 
            this.cittacomp.AutoSize = true;
            this.cittacomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cittacomp.Location = new System.Drawing.Point(50, 160);
            this.cittacomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cittacomp.Name = "cittacomp";
            this.cittacomp.Size = new System.Drawing.Size(42, 18);
            this.cittacomp.TabIndex = 41;
            this.cittacomp.Text = "Città:";
            // 
            // TxtProvincia
            // 
            this.TxtProvincia.Location = new System.Drawing.Point(291, 195);
            this.TxtProvincia.Margin = new System.Windows.Forms.Padding(4);
            this.TxtProvincia.Name = "TxtProvincia";
            this.TxtProvincia.Size = new System.Drawing.Size(62, 24);
            this.TxtProvincia.TabIndex = 40;
            // 
            // provinciacomp
            // 
            this.provinciacomp.AutoSize = true;
            this.provinciacomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.provinciacomp.Location = new System.Drawing.Point(210, 198);
            this.provinciacomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.provinciacomp.Name = "provinciacomp";
            this.provinciacomp.Size = new System.Drawing.Size(73, 18);
            this.provinciacomp.TabIndex = 39;
            this.provinciacomp.Text = "Provincia:";
            // 
            // TxtCap
            // 
            this.TxtCap.BackColor = System.Drawing.SystemColors.Window;
            this.TxtCap.Location = new System.Drawing.Point(114, 195);
            this.TxtCap.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCap.Name = "TxtCap";
            this.TxtCap.Size = new System.Drawing.Size(88, 24);
            this.TxtCap.TabIndex = 38;
            // 
            // capcomp
            // 
            this.capcomp.AutoSize = true;
            this.capcomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capcomp.Location = new System.Drawing.Point(64, 198);
            this.capcomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.capcomp.Name = "capcomp";
            this.capcomp.Size = new System.Drawing.Size(42, 18);
            this.capcomp.TabIndex = 37;
            this.capcomp.Text = "CAP:";
            // 
            // TxtIndirizzo
            // 
            this.TxtIndirizzo.Location = new System.Drawing.Point(100, 116);
            this.TxtIndirizzo.Margin = new System.Windows.Forms.Padding(4);
            this.TxtIndirizzo.Name = "TxtIndirizzo";
            this.TxtIndirizzo.Size = new System.Drawing.Size(253, 24);
            this.TxtIndirizzo.TabIndex = 36;
            // 
            // indirizzocomp
            // 
            this.indirizzocomp.AutoSize = true;
            this.indirizzocomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.indirizzocomp.Location = new System.Drawing.Point(25, 119);
            this.indirizzocomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.indirizzocomp.Name = "indirizzocomp";
            this.indirizzocomp.Size = new System.Drawing.Size(67, 18);
            this.indirizzocomp.TabIndex = 35;
            this.indirizzocomp.Text = "Indirizzo:";
            // 
            // TxtNome
            // 
            this.TxtNome.Location = new System.Drawing.Point(185, 76);
            this.TxtNome.Margin = new System.Windows.Forms.Padding(4);
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.Size = new System.Drawing.Size(168, 24);
            this.TxtNome.TabIndex = 34;
            // 
            // nomecomp
            // 
            this.nomecomp.AutoSize = true;
            this.nomecomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomecomp.Location = new System.Drawing.Point(13, 76);
            this.nomecomp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nomecomp.Name = "nomecomp";
            this.nomecomp.Size = new System.Drawing.Size(164, 18);
            this.nomecomp.TabIndex = 33;
            this.nomecomp.Text = "Nome della compagnia:";
            // 
            // datiacq
            // 
            this.datiacq.AutoSize = true;
            this.datiacq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datiacq.Location = new System.Drawing.Point(83, 25);
            this.datiacq.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.datiacq.Name = "datiacq";
            this.datiacq.Size = new System.Drawing.Size(235, 29);
            this.datiacq.TabIndex = 32;
            this.datiacq.Text = "DATI ACQUIRENTI";
            // 
            // btncinferma2
            // 
            this.btncinferma2.AllowDrop = true;
            this.btncinferma2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncinferma2.Location = new System.Drawing.Point(269, 280);
            this.btncinferma2.Margin = new System.Windows.Forms.Padding(4);
            this.btncinferma2.Name = "btncinferma2";
            this.btncinferma2.Size = new System.Drawing.Size(84, 27);
            this.btncinferma2.TabIndex = 47;
            this.btncinferma2.Text = "Conferma";
            this.btncinferma2.UseVisualStyleBackColor = true;
            this.btncinferma2.Click += new System.EventHandler(this.btnavanti3_Click);
            // 
            // bntannulla
            // 
            this.bntannulla.AllowDrop = true;
            this.bntannulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntannulla.Location = new System.Drawing.Point(177, 280);
            this.bntannulla.Margin = new System.Windows.Forms.Padding(4);
            this.bntannulla.Name = "bntannulla";
            this.bntannulla.Size = new System.Drawing.Size(84, 27);
            this.bntannulla.TabIndex = 48;
            this.bntannulla.Text = "Annulla";
            this.bntannulla.UseVisualStyleBackColor = true;
            this.bntannulla.Click += new System.EventHandler(this.bntannulla_Click);
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.Location = new System.Drawing.Point(293, 296);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Size = new System.Drawing.Size(100, 100);
            this.printPreviewControl1.TabIndex = 49;
            // 
            // domodatabaseacquirenti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 316);
            this.Controls.Add(this.printPreviewControl1);
            this.Controls.Add(this.bntannulla);
            this.Controls.Add(this.btncinferma2);
            this.Controls.Add(this.TxtLongitudine);
            this.Controls.Add(this.barra);
            this.Controls.Add(this.TxtLatitudine);
            this.Controls.Add(this.posizionegpscomp);
            this.Controls.Add(this.TxtCitta);
            this.Controls.Add(this.cittacomp);
            this.Controls.Add(this.TxtProvincia);
            this.Controls.Add(this.provinciacomp);
            this.Controls.Add(this.TxtCap);
            this.Controls.Add(this.capcomp);
            this.Controls.Add(this.TxtIndirizzo);
            this.Controls.Add(this.indirizzocomp);
            this.Controls.Add(this.TxtNome);
            this.Controls.Add(this.nomecomp);
            this.Controls.Add(this.datiacq);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "domodatabaseacquirenti";
            this.Text = "DomoDatabase";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.domodatabaseacquirenti_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label barra;
        private System.Windows.Forms.Label posizionegpscomp;
        private System.Windows.Forms.Label cittacomp;
        private System.Windows.Forms.Label provinciacomp;
        private System.Windows.Forms.Label capcomp;
        private System.Windows.Forms.Label indirizzocomp;
        private System.Windows.Forms.Label nomecomp;
        private System.Windows.Forms.Label datiacq;
        public System.Windows.Forms.TextBox TxtNome;
        public System.Windows.Forms.TextBox TxtLongitudine;
        public System.Windows.Forms.TextBox TxtLatitudine;
        public System.Windows.Forms.TextBox TxtCitta;
        public System.Windows.Forms.TextBox TxtProvincia;
        public System.Windows.Forms.TextBox TxtCap;
        public System.Windows.Forms.TextBox TxtIndirizzo;
        private System.Windows.Forms.Button btncinferma2;
        private System.Windows.Forms.Button bntannulla;
        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
    }
}