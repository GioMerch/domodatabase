﻿namespace DomoDatabase
{
    partial class frmGUI
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStartConn = new System.Windows.Forms.Button();
            this.btnStopConn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCOMport = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNodes = new System.Windows.Forms.TextBox();
            this.btnStartSimplified = new System.Windows.Forms.Button();
            this.btnStartLog = new System.Windows.Forms.Button();
            this.btnStopLog = new System.Windows.Forms.Button();
            this.svDiag = new System.Windows.Forms.SaveFileDialog();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.cbkScroll = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStartConn);
            this.groupBox1.Controls.Add(this.btnStopConn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbCOMport);
            this.groupBox1.Location = new System.Drawing.Point(279, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dati Connessione";
            // 
            // btnStartConn
            // 
            this.btnStartConn.Location = new System.Drawing.Point(6, 46);
            this.btnStartConn.Name = "btnStartConn";
            this.btnStartConn.Size = new System.Drawing.Size(64, 23);
            this.btnStartConn.TabIndex = 3;
            this.btnStartConn.Text = "Connect";
            this.btnStartConn.UseVisualStyleBackColor = true;
            this.btnStartConn.Click += new System.EventHandler(this.btnStartConn_Click);
            // 
            // btnStopConn
            // 
            this.btnStopConn.Enabled = false;
            this.btnStopConn.Location = new System.Drawing.Point(76, 46);
            this.btnStopConn.Name = "btnStopConn";
            this.btnStopConn.Size = new System.Drawing.Size(75, 23);
            this.btnStopConn.TabIndex = 2;
            this.btnStopConn.Text = "Disconnect";
            this.btnStopConn.UseVisualStyleBackColor = true;
            this.btnStopConn.Click += new System.EventHandler(this.btnStopConn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Porta COM:";
            // 
            // cmbCOMport
            // 
            this.cmbCOMport.FormattingEnabled = true;
            this.cmbCOMport.Location = new System.Drawing.Point(76, 19);
            this.cmbCOMport.Name = "cmbCOMport";
            this.cmbCOMport.Size = new System.Drawing.Size(78, 21);
            this.cmbCOMport.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtNodes);
            this.groupBox2.Controls.Add(this.btnStartSimplified);
            this.groupBox2.Controls.Add(this.btnStartLog);
            this.groupBox2.Controls.Add(this.btnStopLog);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(261, 101);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Indirizzi dei nodi";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // txtNodes
            // 
            this.txtNodes.Location = new System.Drawing.Point(9, 15);
            this.txtNodes.Name = "txtNodes";
            this.txtNodes.ReadOnly = true;
            this.txtNodes.Size = new System.Drawing.Size(239, 20);
            this.txtNodes.TabIndex = 2;
            // 
            // btnStartSimplified
            // 
            this.btnStartSimplified.Location = new System.Drawing.Point(30, 70);
            this.btnStartSimplified.Name = "btnStartSimplified";
            this.btnStartSimplified.Size = new System.Drawing.Size(194, 23);
            this.btnStartSimplified.TabIndex = 6;
            this.btnStartSimplified.Text = "Start Logging semplificato";
            this.btnStartSimplified.UseVisualStyleBackColor = true;
            // 
            // btnStartLog
            // 
            this.btnStartLog.Location = new System.Drawing.Point(9, 41);
            this.btnStartLog.Name = "btnStartLog";
            this.btnStartLog.Size = new System.Drawing.Size(118, 23);
            this.btnStartLog.TabIndex = 3;
            this.btnStartLog.Text = "Start Logging";
            this.btnStartLog.UseVisualStyleBackColor = true;
            this.btnStartLog.Click += new System.EventHandler(this.btnStartLog_Click);
            // 
            // btnStopLog
            // 
            this.btnStopLog.Location = new System.Drawing.Point(130, 41);
            this.btnStopLog.Name = "btnStopLog";
            this.btnStopLog.Size = new System.Drawing.Size(118, 23);
            this.btnStopLog.TabIndex = 4;
            this.btnStopLog.Text = "Pause Logging";
            this.btnStopLog.UseVisualStyleBackColor = true;
            this.btnStopLog.Click += new System.EventHandler(this.btnStopLog_Click);
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Location = new System.Drawing.Point(12, 119);
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(543, 292);
            this.txtLog.TabIndex = 2;
            this.txtLog.Text = "";
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // cbkScroll
            // 
            this.cbkScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbkScroll.AutoSize = true;
            this.cbkScroll.Checked = true;
            this.cbkScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbkScroll.Location = new System.Drawing.Point(606, 167);
            this.cbkScroll.Name = "cbkScroll";
            this.cbkScroll.Size = new System.Drawing.Size(71, 17);
            this.cbkScroll.TabIndex = 5;
            this.cbkScroll.Text = "autoscroll";
            this.cbkScroll.UseVisualStyleBackColor = true;
            // 
            // frmGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 414);
            this.Controls.Add(this.cbkScroll);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmGUI";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStartConn;
        private System.Windows.Forms.Button btnStopConn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCOMport;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SaveFileDialog svDiag;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Button btnStartLog;
        private System.Windows.Forms.Button btnStopLog;
        private System.Windows.Forms.CheckBox cbkScroll;
        private System.Windows.Forms.TextBox txtNodes;
        private System.Windows.Forms.Button btnStartSimplified;
    }
}

