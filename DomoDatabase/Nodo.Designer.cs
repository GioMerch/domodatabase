﻿namespace DomoDatabase
{
    partial class domodatabasenodi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConferma4 = new System.Windows.Forms.Button();
            this.TxtNote = new System.Windows.Forms.TextBox();
            this.insnote = new System.Windows.Forms.Label();
            this.TxtPosizionenodo = new System.Windows.Forms.TextBox();
            this.insposizionenodo = new System.Windows.Forms.Label();
            this.TxtNumeronodo = new System.Windows.Forms.TextBox();
            this.insnumeronodo = new System.Windows.Forms.Label();
            this.configurazionenodi = new System.Windows.Forms.Label();
            this.insnuovonodo = new System.Windows.Forms.Button();
            this.listanodi = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnConferma4
            // 
            this.btnConferma4.Location = new System.Drawing.Point(228, 176);
            this.btnConferma4.Name = "btnConferma4";
            this.btnConferma4.Size = new System.Drawing.Size(75, 23);
            this.btnConferma4.TabIndex = 17;
            this.btnConferma4.Text = "Conferma";
            this.btnConferma4.UseVisualStyleBackColor = true;
            this.btnConferma4.Click += new System.EventHandler(this.btnConferma4_Click);
            // 
            // TxtNote
            // 
            this.TxtNote.Location = new System.Drawing.Point(120, 95);
            this.TxtNote.Multiline = true;
            this.TxtNote.Name = "TxtNote";
            this.TxtNote.Size = new System.Drawing.Size(196, 64);
            this.TxtNote.TabIndex = 16;
            // 
            // insnote
            // 
            this.insnote.AutoSize = true;
            this.insnote.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insnote.Location = new System.Drawing.Point(82, 96);
            this.insnote.Name = "insnote";
            this.insnote.Size = new System.Drawing.Size(36, 15);
            this.insnote.TabIndex = 15;
            this.insnote.Text = "Note:";
            // 
            // TxtPosizionenodo
            // 
            this.TxtPosizionenodo.Location = new System.Drawing.Point(120, 69);
            this.TxtPosizionenodo.Name = "TxtPosizionenodo";
            this.TxtPosizionenodo.Size = new System.Drawing.Size(101, 20);
            this.TxtPosizionenodo.TabIndex = 14;
            // 
            // insposizionenodo
            // 
            this.insposizionenodo.AutoSize = true;
            this.insposizionenodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insposizionenodo.Location = new System.Drawing.Point(3, 70);
            this.insposizionenodo.Name = "insposizionenodo";
            this.insposizionenodo.Size = new System.Drawing.Size(115, 15);
            this.insposizionenodo.TabIndex = 13;
            this.insposizionenodo.Text = "Posizione del nodo:";
            // 
            // TxtNumeronodo
            // 
            this.TxtNumeronodo.Location = new System.Drawing.Point(120, 43);
            this.TxtNumeronodo.Name = "TxtNumeronodo";
            this.TxtNumeronodo.Size = new System.Drawing.Size(31, 20);
            this.TxtNumeronodo.TabIndex = 12;
            this.TxtNumeronodo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // insnumeronodo
            // 
            this.insnumeronodo.AutoSize = true;
            this.insnumeronodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insnumeronodo.Location = new System.Drawing.Point(12, 44);
            this.insnumeronodo.Name = "insnumeronodo";
            this.insnumeronodo.Size = new System.Drawing.Size(106, 15);
            this.insnumeronodo.TabIndex = 11;
            this.insnumeronodo.Text = "Numero del nodo:";
            // 
            // configurazionenodi
            // 
            this.configurazionenodi.AutoSize = true;
            this.configurazionenodi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurazionenodi.Location = new System.Drawing.Point(46, 9);
            this.configurazionenodi.Name = "configurazionenodi";
            this.configurazionenodi.Size = new System.Drawing.Size(230, 24);
            this.configurazionenodi.TabIndex = 10;
            this.configurazionenodi.Text = "CONFIGURAZIONE NODI";
            // 
            // insnuovonodo
            // 
            this.insnuovonodo.Location = new System.Drawing.Point(97, 176);
            this.insnuovonodo.Name = "insnuovonodo";
            this.insnuovonodo.Size = new System.Drawing.Size(125, 23);
            this.insnuovonodo.TabIndex = 19;
            this.insnuovonodo.Text = "Inserire  nodo";
            this.insnuovonodo.UseVisualStyleBackColor = true;
            this.insnuovonodo.Click += new System.EventHandler(this.insnuovonodo_Click);
            // 
            // listanodi
            // 
            this.listanodi.FormattingEnabled = true;
            this.listanodi.Location = new System.Drawing.Point(333, 9);
            this.listanodi.Name = "listanodi";
            this.listanodi.Size = new System.Drawing.Size(281, 186);
            this.listanodi.TabIndex = 20;
            // 
            // domodatabasenodi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 209);
            this.Controls.Add(this.listanodi);
            this.Controls.Add(this.insnuovonodo);
            this.Controls.Add(this.btnConferma4);
            this.Controls.Add(this.TxtNote);
            this.Controls.Add(this.insnote);
            this.Controls.Add(this.TxtPosizionenodo);
            this.Controls.Add(this.insposizionenodo);
            this.Controls.Add(this.TxtNumeronodo);
            this.Controls.Add(this.insnumeronodo);
            this.Controls.Add(this.configurazionenodi);
            this.Name = "domodatabasenodi";
            this.Text = "DomoDatabase";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.domodatabasenodi_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnConferma4;
        private System.Windows.Forms.TextBox TxtNote;
        private System.Windows.Forms.Label insnote;
        private System.Windows.Forms.TextBox TxtPosizionenodo;
        private System.Windows.Forms.Label insposizionenodo;
        private System.Windows.Forms.Label insnumeronodo;
        private System.Windows.Forms.Label configurazionenodi;
        private System.Windows.Forms.Button insnuovonodo;
        private System.Windows.Forms.ListBox listanodi;
        public System.Windows.Forms.TextBox TxtNumeronodo;
    }
}