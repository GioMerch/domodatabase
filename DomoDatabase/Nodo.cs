﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DomoDatabase
{
    public partial class domodatabasenodi : Form
    {
        public System.Collections.ArrayList numeronodi = new System.Collections.ArrayList();
        public System.Collections.ArrayList posizionenodi = new System.Collections.ArrayList();
        public System.Collections.ArrayList notes = new System.Collections.ArrayList();
        public System.Collections.ArrayList datinodi = new System.Collections.ArrayList();
        public int numeronodo = 0;
        public String posizionenodo = "";
        public String note = "";
        public String noditot = "";
        int i = 0;
        int e = 0;
        int n = 1;
        bool controllorepeat = false;
        bool inserimentivuoti = true;

        public domodatabasenodi()
        {
            InitializeComponent();
            numnodo();
        }

        public void Insnodi()
        {
            foreach (String _datonodo in globals.frmNodi.datinodi)
            {
                globals.frmNodi.noditot = _datonodo;
                e++;
                foreach (int _numeronodo in globals.frmNodi.numeronodi)
                {
                    globals.frmNodi.numeronodo = _numeronodo;
                    foreach (String _posizionenodo in globals.frmNodi.posizionenodi)
                    {
                        globals.frmNodi.posizionenodo = _posizionenodo;
                        foreach (String _note in globals.frmNodi.notes)
                        {
                            globals.frmNodi.note = _note;
                            if ((globals.frmNodi.numeronodo + globals.frmNodi.posizionenodo + globals.frmNodi.note) == globals.frmNodi.noditot && e == n)
                            {
                                n++;
                                try
                                {
                                    string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                                    string strMiaQuery = "INSERT INTO nodi(indirizzo,posizione,note,periodo) VALUES(@indirizzo,@posizione,@note,@ID_periodo)";
                                    MySqlConnection myConn = new MySqlConnection(mySelectConn);
                                    MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                                    myConn.Open();
                                    myCmd.Parameters.AddWithValue("@indirizzo", globals.frmNodi.numeronodo);
                                    myCmd.Parameters.AddWithValue("@posizione", globals.frmNodi.posizionenodo);
                                    myCmd.Parameters.AddWithValue("@note", globals.frmNodi.note);
                                    myCmd.Parameters.AddWithValue("@ID_periodo", globals.frmPeriodi.ID_periodo);
                                    myCmd.ExecuteNonQuery();
                                    myConn.Close();
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show("Problemi di connesione al database.");
                                }
                            }
                        }
                    }
                }
            }
        }

        public void numnodo()
        {
            TxtNumeronodo.Text = "";
            i++;
            TxtNumeronodo.Text = "";
            TxtNumeronodo.Text = TxtNumeronodo.Text + i;
        }

        public void Visnodo()
        {
            listanodi.Text = "";
            foreach (int num in globals.frmNodi.numeronodi)
            {
                try
                {
                    if (Convert.ToInt32(TxtNumeronodo.Text) == num)
                    {
                        MessageBox.Show("Nodo già registrato!");
                        controllorepeat = true;
                        i = Convert.ToInt32(TxtNumeronodo.Text);
                        TxtNumeronodo.Text = "";
                        i++;
                        TxtNumeronodo.Text = TxtNumeronodo.Text + i;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Inserire il nodo corretto!");
                    controllorepeat = true;
                    TxtNumeronodo.Text = "";
                    TxtNumeronodo.Text = TxtNumeronodo.Text + i;
                    break;
                }
            }
            if(controllorepeat==false)
            {
                try
                {
                    if(TxtPosizionenodo.Text != "")
                    {
                        globals.frmNodi.numeronodo = Convert.ToInt32(TxtNumeronodo.Text);
                        globals.frmNodi.posizionenodo = TxtPosizionenodo.Text;
                        if(TxtNote.Text=="")
                        {
                            globals.frmNodi.note = "---";
                        }
                        else
                        {
                            globals.frmNodi.note = TxtNote.Text;
                        }
                        listanodi.Items.Add("N°: " + globals.frmNodi.numeronodo + "\tStanza: " + globals.frmNodi.posizionenodo + "\tNote: " + globals.frmNodi.note + Environment.NewLine);
                        globals.frmNodi.numeronodi.Add(globals.frmNodi.numeronodo);
                        globals.frmNodi.posizionenodi.Add(globals.frmNodi.posizionenodo);
                        globals.frmNodi.notes.Add(globals.frmNodi.note);
                        globals.frmNodi.datinodi.Add(globals.frmNodi.numeronodo + globals.frmNodi.posizionenodo + globals.frmNodi.note);
                        if (Convert.ToInt32(TxtNumeronodo.Text) > i || Convert.ToInt32(TxtNumeronodo.Text) < i)
                        {
                            i = Convert.ToInt32(TxtNumeronodo.Text);
                        }
                        numnodo();
                        inserimentivuoti = false;
                    }
                    else
                    {
                        MessageBox.Show("La posizione del nodo non deve essere vuota!");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Inserire i dati corretti!");
                    TxtNumeronodo.Text = "";
                    TxtNumeronodo.Text = TxtNumeronodo.Text + i;
                }
                TxtPosizionenodo.Text = "";
                TxtNote.Text = "";
            }
        }

        private void btnConferma4_Click(object sender, EventArgs e)
        {
            if(inserimentivuoti==false)
            {
                globals.frmPeriodi.ctrl11 = true;
                globals.frmPeriodi.ctrl10 = true;
                globals.frmAcquirenti.ctrl2 = true;
                domodatabaseresoconti frmresoconti = new domodatabaseresoconti();
                Close();
                frmresoconti.Show();
            }
            else
            {
                MessageBox.Show("Inserire almeno un nodo!");
            }
        }

        private void insnuovonodo_Click(object sender, EventArgs e)
        {
            Visnodo();
            controllorepeat = false;
        }

        private void domodatabasenodi_FormClosed(object sender, FormClosedEventArgs e)
        {
            globals.frmPeriodi.ctrl11 = false;
        }
    }
}
