﻿namespace DomoDatabase
{
    partial class IncDati
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.visdatinodo = new System.Windows.Forms.TextBox();
            this.visincdati = new System.Windows.Forms.TextBox();
            this.btnincdati = new System.Windows.Forms.Button();
            this.tmrFakeUpdate = new System.Windows.Forms.Timer(this.components);
            this.btnstop = new System.Windows.Forms.Button();
            this.btnindietro1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // visdatinodo
            // 
            this.visdatinodo.Location = new System.Drawing.Point(12, 12);
            this.visdatinodo.Name = "visdatinodo";
            this.visdatinodo.ReadOnly = true;
            this.visdatinodo.Size = new System.Drawing.Size(758, 20);
            this.visdatinodo.TabIndex = 0;
            // 
            // visincdati
            // 
            this.visincdati.Location = new System.Drawing.Point(12, 48);
            this.visincdati.Multiline = true;
            this.visincdati.Name = "visincdati";
            this.visincdati.ReadOnly = true;
            this.visincdati.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.visincdati.Size = new System.Drawing.Size(758, 203);
            this.visincdati.TabIndex = 1;
            // 
            // btnincdati
            // 
            this.btnincdati.Location = new System.Drawing.Point(695, 257);
            this.btnincdati.Name = "btnincdati";
            this.btnincdati.Size = new System.Drawing.Size(75, 23);
            this.btnincdati.TabIndex = 4;
            this.btnincdati.Text = "Carica dati\r\n";
            this.btnincdati.UseVisualStyleBackColor = true;
            this.btnincdati.Click += new System.EventHandler(this.btnincdati_Click);
            // 
            // tmrFakeUpdate
            // 
            this.tmrFakeUpdate.Interval = 2000;
            this.tmrFakeUpdate.Tick += new System.EventHandler(this.tmrFakeUpdate_Tick);
            // 
            // btnstop
            // 
            this.btnstop.Location = new System.Drawing.Point(614, 257);
            this.btnstop.Name = "btnstop";
            this.btnstop.Size = new System.Drawing.Size(75, 23);
            this.btnstop.TabIndex = 5;
            this.btnstop.Text = "Stop";
            this.btnstop.UseVisualStyleBackColor = true;
            this.btnstop.Click += new System.EventHandler(this.btnstop_Click);
            // 
            // btnindietro1
            // 
            this.btnindietro1.Location = new System.Drawing.Point(533, 257);
            this.btnindietro1.Name = "btnindietro1";
            this.btnindietro1.Size = new System.Drawing.Size(75, 23);
            this.btnindietro1.TabIndex = 6;
            this.btnindietro1.Text = "Indietro";
            this.btnindietro1.UseVisualStyleBackColor = true;
            this.btnindietro1.Click += new System.EventHandler(this.btnindietro1_Click);
            // 
            // IncDati
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 287);
            this.Controls.Add(this.btnindietro1);
            this.Controls.Add(this.btnstop);
            this.Controls.Add(this.btnincdati);
            this.Controls.Add(this.visincdati);
            this.Controls.Add(this.visdatinodo);
            this.Name = "IncDati";
            this.Text = "IncDati";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IncDati_FormClosed);
            this.Load += new System.EventHandler(this.IncDati_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox visdatinodo;
        private System.Windows.Forms.TextBox visincdati;
        private System.Windows.Forms.Button btnincdati;
        private System.Windows.Forms.Timer tmrFakeUpdate;
        private System.Windows.Forms.Button btnstop;
        private System.Windows.Forms.Button btnindietro1;
    }
}