﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DomoDatabase
{



    static class Program
    {

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if MODBUS||RASP
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            globals.frmIncDati1 = new frmGUI();
            Application.Run(globals.frmIncDati1);
#else
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new domodatabaseperiodi());

#endif
        }
    }

}
