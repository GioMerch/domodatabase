﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DomoDatabase
{
    public partial class IncNodo : Form
    {
        public int ID_nodo = 0;
        public int numeronodo = 0;
        public String posizionenodo = "";
        public String note = "";

        public IncNodo()
        {
            InitializeComponent();
        }

        public void Combonodo()
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT id,indirizzo,posizione,periodo FROM nodi";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int ID = 0;
                int numeronodo = 0;
                String posizionenodo = "";
                int ID_periodo = 0;
                while (myReader.Read())
                {
                    ID = myReader.GetInt32(0);
                    numeronodo = myReader.GetInt32(1);
                    posizionenodo = myReader.GetString(2);
                    ID_periodo = myReader.GetInt32(3);
                    if (ID_periodo == globals.frmIncPeriodo.ID_periodo)
                    {
                        globals.frmIncNodo.ID_nodo = ID;
                        globals.frmIncNodo.numeronodo = numeronodo;
                        globals.frmIncNodo.posizionenodo = posizionenodo;
                        nodoacq.Items.Add(globals.frmIncNodo.numeronodo + ") " + globals.frmIncNodo.posizionenodo);
                    }
                }
                myReader.Close();
                myConn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        private void BtnIncNodo_Click(object sender, EventArgs e)
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT * FROM nodi";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int ID_nodo = 0;
                int numeronodo = 0;
                String posizionenodo = "";
                String note = "";
                int ID_periodo = 0;
                bool i = true;
                while (i)
                {
                    myReader.Read();
                    ID_nodo = myReader.GetInt32(0);
                    numeronodo = myReader.GetInt32(1);
                    posizionenodo = myReader.GetString(2);
                    note = myReader.GetString(3);
                    ID_periodo = myReader.GetInt32(4);
                    if (ID_periodo == globals.frmIncPeriodo.ID_periodo && nodoacq.SelectedItem.Equals(numeronodo + ") " + posizionenodo))
                    {
                        i = false;
                        globals.frmIncPeriodo.ID_nodo = ID_nodo;
                        globals.frmNodi.numeronodo = numeronodo;
                        globals.frmNodi.posizionenodo = posizionenodo;
                        globals.frmNodi.note = note;
                        globals.frmPeriodi.ID_periodo = ID_periodo;
                    }
                }
                myReader.Close();
                myConn.Close();
                IncDati incdati = new IncDati();
                //globals.frmIncDati.Show();
                Close();
                incdati.Show();
                globals.frmPeriodi.ctrl10 = true;
                globals.frmAcquirenti.ctrl2 = true;
                globals.frmPeriodi.ctrl11 = true;
            }
            catch
            {
                MessageBox.Show("Inserire il nodo!");
            }
        }

        private void IncNodo_Load(object sender, EventArgs e)
        {
            globals.frmPeriodi.ctrl11 = true;
            globals.frmPeriodi.ctrl10 = true;
            globals.frmAcquirenti.ctrl2 = true;
            visperiodoacq.Text = visperiodoacq.Text + "ID del periodo: " + globals.frmIncPeriodo.ID_periodo + "\tNumero di installazione: " + globals.frmIncPeriodo.numero_di_installazione + "\tData di inizio: " + globals.frmIncPeriodo.start + Environment.NewLine;
            Combonodo();
        }

        private void IncNodo_FormClosed(object sender, FormClosedEventArgs e)
        {
            globals.frmPeriodi.ctrl10 = false;
        }

        private void btnindietro2_Click(object sender, EventArgs e)
        {
            IncPeriodo incperiodo = new IncPeriodo();
            Close();
            globals.frmPeriodi.ctrl11 = true;
            globals.frmAcquirenti.ctrl2 = true;
            globals.frmPeriodi.ctrl10 = true;
            incperiodo.Show();
        }
    }
}
