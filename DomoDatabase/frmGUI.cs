﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using MySql.Data.MySqlClient;
using Carlo;

namespace DomoDatabase
{
    public partial class frmGUI : Form
    {

        public frmGUI()
        {
            InitializeComponent();
#if RASP
            DatiDaFile();
#endif
            inizializzazione();
#if RASP
            //btnStartConn_Click(null, null);
            btnStartLog_Click(null, null);
#endif
        }

        public delegate void delegAddLog();
        public delegAddLog myDelegate;
        public List<IncNodo> IDnodi = new List<IncNodo>();
        int TimestampLastInsert;
        List<dataRecord> raccolti = new List<dataRecord>();
        bool ctrl = false;


        public ThreadControlloFile threadFile = new ThreadControlloFile();

        Thread controlloModFile;
        Thread modbusRunner;

        public void inizializzazione()
        {
            txtLog.WordWrap = true;
            myDelegate = new delegAddLog(addLogRecord);

            globals.modbusThread = new ModbusThread();
            globals.modbusThread.connectedNodes = new List<int>();

            globals.modbusThread.caronte = myDelegate;

            String[] postList = System.IO.Ports.SerialPort.GetPortNames();
            
            cmbCOMport.Items.Clear();
            foreach (string port in postList)
            {
                cmbCOMport.Items.Add(port);
            }
            if (postList.Length == 1)
            {
                cmbCOMport.SelectedIndex = 0;
            }

            globals.modbusThread.modbusSerial = this.serialModbus;

        }

        TAGLaser.ModBus.modbusSerial serialModbus;

        private void btnStartConn_Click(object sender, EventArgs e)
        {
            btnStopConn.Enabled = true;
            btnStartConn.Enabled = false;

            logEvent("Instauro connessione...", Color.Red);


            serialModbus = new TAGLaser.ModBus.modbusSerial();
            string COMport = cmbCOMport.Text;
            bool connResult = serialModbus.Open(COMport, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);


            if(connResult)
                logEvent("...fatto", Color.Green);
            else
                logEvent("problemi in apertura connessione\n"+serialModbus.modbusStatus, Color.Red);

            globals.modbusThread.modbusSerial = serialModbus;
        }

        private void btnStopConn_Click(object sender, EventArgs e)
        {
            //codice per fermare la connessione
            bool connResult = serialModbus.Close();
            if (connResult)
                logEvent("...connessione chiusa", Color.Green);
            else
                logEvent("problemi in chiusura connessione", Color.Red);
            btnStopConn.Enabled = false;
            btnStartConn.Enabled = true;
        }
        

        private void logEvent(string testo, Color colore)
        {
            DateTime dataora = DateTime.Now;
            string datetime = dataora.ToString("HH:mm:ss");
            if (!txtLog.IsDisposed)
            {
                txtLog.SelectionColor = Color.Black;
                txtLog.AppendText("[");
                txtLog.AppendText(datetime);
                txtLog.AppendText("] ");
                txtLog.SelectionColor = colore;
                txtLog.AppendText(testo);
                txtLog.AppendText("\r\n");
                txtLog.SelectionColor = Color.Black;
            }
        }
#if !MODBUS && !RASP
        private void Form1_Load(object sender, EventArgs e)
        {
            if(globals.frmResoconti.controlloalproxform == true)
            {
                this.txtNodes.Text = "";
                string str = "";
                foreach (IncNodo n in globals.frmIncDati1.IDnodi)
                {
                    str += n.numeronodo + ",";
                }
                this.txtNodes.Text = str.Remove(str.Length - 1);
                globals.frmResoconti.controlloalproxform = false;
            }
            else
            {
                Visnumnodi();
            }
        }
#else
        private void Form1_Load(object sender, EventArgs e)
        {
        }
#endif


        private void btnStartLog_Click(object sender, EventArgs e)
        {
            //creare il file di log:
            //File.AppendAllText(txtOutFile.Text, "LOGFILE\r\n");
            txtSetNodes_Click();

            modbusRunner = new Thread(new ThreadStart(globals.modbusThread.loop));
            modbusRunner.Start();
        }

        public void addLogRecord()
        {
            //globals.modbusThread.idle = true;

            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(this.addLogRecord));
                return;

            }
            logEvent("callback from thread", Color.Aqua);
            foreach(dataRecord rec in globals.modbusThread.ultimaLettura)
                logEvent("" + rec, Color.BlueViolet);
            /*foreach (string rec in globals.modbusThread.stringhe)
                logEvent(rec, Color.BlueViolet);*/

                //write line in CSV
                //if (File.Exists(txtOutFile.Text))
                //{
                //    String line = "";
                //    foreach (dataRecord rec in globals.modbusThread.ultimaLettura)
                //    {
                //        line += rec.node + ";";
                //        line += rec.timeStamp + ";";
                //        line += rec.temperature + ";";
                //        line += rec.humidity + ";";
                //    }
                //    line += "\r\n";
                //    File.AppendAllText(txtOutFile.Text, line);
                //}

                Insnodi();                                                        //Per inserimento nel database

                //globals.modbusThread.idle = false;
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {
            if (cbkScroll.Checked)
            {
                // set the current caret position to the end
                txtLog.SelectionStart = txtLog.Text.Length;
                // scroll it automatically
                txtLog.ScrollToCaret();
            }
        }

        private void btnStopLog_Click(object sender, EventArgs e)
        {
            globals.modbusThread.idle = !globals.modbusThread.idle;
            if (globals.modbusThread.idle)
            {
                btnStopConn.Text = "resume logging";
            }
            else
            {
                btnStopConn.Text = "pause logging";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            globals.modbusThread.looping = false;
            threadFile.looping = false;
            btnStopLog_Click(null, null);
            //modbusRunner.Join();
#if !MODBUS && !RASP
            globals.frmPeriodi.apribtn();
#endif
        }

        public void Visnumnodi()
        {
            this.txtNodes.Text = "";
            string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
            string strMiaQuery = "SELECT id,indirizzo,posizione,periodo FROM nodi";
            MySqlConnection myConn = new MySqlConnection(mySelectConn);
            MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
            myConn.Open();
            MySqlDataReader myReader = myCmd.ExecuteReader();
            int ID = 0;
            int numeronodo = 0;
            String posizionenodo = "";
            int ID_periodo = 0;
            bool ctrl = false;
            
            while (myReader.Read())
            {
                ID = myReader.GetInt32(0);
                numeronodo = myReader.GetInt32(1);
                posizionenodo = myReader.GetString(2);
                ID_periodo = myReader.GetInt32(3);
                if (ID_periodo == globals.frmIncPeriodo.ID_periodo)
                {
                    IncNodo nodo = new IncNodo();
                    nodo.ID_nodo = ID;
                    nodo.numeronodo = numeronodo;
                    nodo.posizionenodo = posizionenodo;
                    globals.frmIncDati1.IDnodi.Add(nodo);
                    if(ctrl==false)
                    {
                        this.txtNodes.Text = this.txtNodes.Text + nodo.numeronodo;
                    }
                    else
                    {
                        this.txtNodes.Text = this.txtNodes.Text + ", " + nodo.numeronodo;
                    }
                    ctrl = true;
                }
            }
            myReader.Close();
            myConn.Close();
        }

        public void Insnodi()
        {
            if(ctrl == false)
            {
                TimestampLastInsert = System.Environment.TickCount;
                ctrl = true;
            }
            foreach (dataRecord n in globals.modbusThread.ultimaLettura)
            {
                raccolti.Add(n);
            }
     
            int msPassati = System.Environment.TickCount - TimestampLastInsert;

            if (msPassati > 3000 && raccolti.Count > 0)
            {
                TimestampLastInsert = System.Environment.TickCount;

                string mySelectConn = @"server=" + DomoDatabase.globals.serverAddress + ";userid=" + DomoDatabase.globals.userid + ";password=" + DomoDatabase.globals.password + ";database=" + DomoDatabase.globals.database;
                string strMiaQuery = "INSERT into dati (ora,temperatura,umidita,nodo,analogico1,analogico2) values (@orario,@temperatura,@umidita,@ID_posizione_nodo,@A_1,@A_2)";
                //string result = mysql_query(strMiaQuery, mySelectConn) or die(mysql_error());
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                try
                {
                    myConn.Open();
                    using (MySqlTransaction trans = myConn.BeginTransaction())
                    {
                        using (myCmd = new MySqlCommand(strMiaQuery, myConn, trans))
                        {
                            myCmd.CommandType = CommandType.Text;
                            foreach (dataRecord n in raccolti)
                            {
                                myCmd.Parameters.Clear();
                                myCmd.Parameters.AddWithValue("@orario", n.timeStamp);
                                myCmd.Parameters.AddWithValue("@temperatura", (float)n.temperature / 10.0f);
                                myCmd.Parameters.AddWithValue("@umidita", (float)n.humidity / 10.0f);
                                myCmd.Parameters.AddWithValue("@A_1", n.analogic_1);
                                myCmd.Parameters.AddWithValue("@A_2", n.analogic_2);
                                myCmd.Parameters.AddWithValue("@ID_posizione_nodo", FindNode(n.node));
                                myCmd.ExecuteNonQuery();
                            }
                            trans.Commit();
                        }
                    }
                    logEvent("___________Salvataggio nel Database______________", Color.Blue);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    myConn.Close();
                    
                }
                
                raccolti.Clear();
            }
        }

        //restituisce l'ID del nodo in base al suo numero
        public int FindNode(int n)
        {
#if !RASP
            foreach (IncNodo nodo in globals.frmIncDati1.IDnodi)
#else
            foreach (IncNodo nodo in IDnodi)
#endif
            {
                if(n == nodo.numeronodo)
                {
                    return nodo.ID_nodo;
                }
            }
            throw new Exception("Numero nodo non corrispondente a nessun ID.");
        }

        private void txtSetNodes_Click()
        {
            String[] nodes = txtNodes.Text.ToString().Split(new char[] { ',' });
            globals.modbusThread.connectedNodes.Clear();

            foreach (string n in nodes)
            {
                if(n!="")
                    globals.modbusThread.connectedNodes.Add(Int32.Parse(n));
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        public void DatiDaFile()
        {
            try
            {
                threadFile.t = File.GetLastWriteTime(AppDomain.CurrentDomain.BaseDirectory + "config.bin");
                controlloModFile = new Thread(new ThreadStart(threadFile.ControlloFile));
                controlloModFile.Start();


                SetDatabase();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                DictionaryUtility.DictionaryFromFile(AppDomain.CurrentDomain.BaseDirectory + "config.bin", out dic);

                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT id,indirizzo FROM nodi WHERE periodo="+dic["periodo"];
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                
                bool ctrl = false;

                while (myReader.Read())
                {
                    IncNodo nodo = new IncNodo();
                    nodo.ID_nodo = myReader.GetInt32(0);
                    nodo.numeronodo = myReader.GetInt32(1);
                    IDnodi.Add(nodo);
                    if (ctrl == false)
                    {
                        this.txtNodes.Text = this.txtNodes.Text + nodo.numeronodo;
                    }
                    else
                    {
                        this.txtNodes.Text = this.txtNodes.Text + ", " + nodo.numeronodo;
                    }
                    ctrl = true;
                }
                myReader.Close();
                myConn.Close();

            }
            catch
            {
                MessageBox.Show("File contenente il periodo non trovato.");
            }
        }

        private void SetDatabase()
        {
            /*try
            {
                String[] credenziali = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "config.txt");
                foreach (String n in credenziali)
                {
                    char separatore = '=';
                    String[] pezzi = n.Split(separatore);
                    String var = pezzi[0];
                    String var1 = pezzi[1];
                    var1 = var1.Trim();
                    if (var.Contains("serverAddress"))
                    {
                        globals.serverAddress = var1;
                    }
                    if (var.Contains("userid"))
                    {
                        globals.userid = var1;
                    }
                    if (var.Contains("password"))
                    {
                        globals.password = var1;
                    }
                    if (var.Contains("database"))
                    {
                        globals.database = var1;
                    }
                }
            }
            catch
            {
                MessageBox.Show("File contenente le credenziali non trovato.");
            }*/

            try
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                DictionaryUtility.DictionaryFromFile(AppDomain.CurrentDomain.BaseDirectory + "configdb.bin", out dic);

                globals.serverAddress = dic["serverAddress"];
                globals.userid = dic["userid"];
                globals.password = dic["password"];
                globals.database = dic["database"];
            }
            catch
            {
                MessageBox.Show("File contenente le credenziali non trovato.");
            }
        }
    }
}
