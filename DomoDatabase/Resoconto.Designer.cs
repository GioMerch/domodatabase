﻿namespace DomoDatabase
{
    partial class domodatabaseresoconti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnconferma = new System.Windows.Forms.Button();
            this.visdatinodi = new System.Windows.Forms.TextBox();
            this.daticonfignodi = new System.Windows.Forms.Label();
            this.visdatimonitoraggio = new System.Windows.Forms.TextBox();
            this.datiperiodo = new System.Windows.Forms.Label();
            this.visdatiacq = new System.Windows.Forms.TextBox();
            this.datiacq = new System.Windows.Forms.Label();
            this.anteprima = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnconferma
            // 
            this.btnconferma.Location = new System.Drawing.Point(386, 330);
            this.btnconferma.Name = "btnconferma";
            this.btnconferma.Size = new System.Drawing.Size(75, 23);
            this.btnconferma.TabIndex = 16;
            this.btnconferma.Text = "Conferma";
            this.btnconferma.UseVisualStyleBackColor = true;
            this.btnconferma.Click += new System.EventHandler(this.btnconferma_Click);
            // 
            // visdatinodi
            // 
            this.visdatinodi.Location = new System.Drawing.Point(12, 193);
            this.visdatinodi.Multiline = true;
            this.visdatinodi.Name = "visdatinodi";
            this.visdatinodi.ReadOnly = true;
            this.visdatinodi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.visdatinodi.Size = new System.Drawing.Size(449, 129);
            this.visdatinodi.TabIndex = 15;
            // 
            // daticonfignodi
            // 
            this.daticonfignodi.AutoSize = true;
            this.daticonfignodi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daticonfignodi.Location = new System.Drawing.Point(9, 172);
            this.daticonfignodi.Name = "daticonfignodi";
            this.daticonfignodi.Size = new System.Drawing.Size(70, 18);
            this.daticonfignodi.TabIndex = 14;
            this.daticonfignodi.Text = "Dati nodi:";
            // 
            // visdatimonitoraggio
            // 
            this.visdatimonitoraggio.Location = new System.Drawing.Point(12, 133);
            this.visdatimonitoraggio.Multiline = true;
            this.visdatimonitoraggio.Name = "visdatimonitoraggio";
            this.visdatimonitoraggio.ReadOnly = true;
            this.visdatimonitoraggio.Size = new System.Drawing.Size(449, 36);
            this.visdatimonitoraggio.TabIndex = 13;
            // 
            // datiperiodo
            // 
            this.datiperiodo.AutoSize = true;
            this.datiperiodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datiperiodo.Location = new System.Drawing.Point(9, 112);
            this.datiperiodo.Name = "datiperiodo";
            this.datiperiodo.Size = new System.Drawing.Size(129, 18);
            this.datiperiodo.TabIndex = 12;
            this.datiperiodo.Text = "Dati monitoraggio:";
            // 
            // visdatiacq
            // 
            this.visdatiacq.Location = new System.Drawing.Point(12, 60);
            this.visdatiacq.Multiline = true;
            this.visdatiacq.Name = "visdatiacq";
            this.visdatiacq.ReadOnly = true;
            this.visdatiacq.Size = new System.Drawing.Size(449, 49);
            this.visdatiacq.TabIndex = 11;
            // 
            // datiacq
            // 
            this.datiacq.AutoSize = true;
            this.datiacq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datiacq.Location = new System.Drawing.Point(9, 39);
            this.datiacq.Name = "datiacq";
            this.datiacq.Size = new System.Drawing.Size(110, 18);
            this.datiacq.TabIndex = 10;
            this.datiacq.Text = "Dati acquirente:";
            // 
            // anteprima
            // 
            this.anteprima.AutoSize = true;
            this.anteprima.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anteprima.Location = new System.Drawing.Point(162, 9);
            this.anteprima.Name = "anteprima";
            this.anteprima.Size = new System.Drawing.Size(161, 29);
            this.anteprima.TabIndex = 9;
            this.anteprima.Text = "ANTEPRIMA";
            // 
            // domodatabaseresoconti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 365);
            this.Controls.Add(this.btnconferma);
            this.Controls.Add(this.visdatinodi);
            this.Controls.Add(this.daticonfignodi);
            this.Controls.Add(this.visdatimonitoraggio);
            this.Controls.Add(this.datiperiodo);
            this.Controls.Add(this.visdatiacq);
            this.Controls.Add(this.datiacq);
            this.Controls.Add(this.anteprima);
            this.Name = "domodatabaseresoconti";
            this.Text = "DomoDatabase";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.domodatabaseresoconti_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.domodatabaseresoconti_FormClosed);
            this.Load += new System.EventHandler(this.domodatabaseresoconti_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnconferma;
        private System.Windows.Forms.TextBox visdatinodi;
        private System.Windows.Forms.Label daticonfignodi;
        private System.Windows.Forms.Label datiperiodo;
        private System.Windows.Forms.TextBox visdatiacq;
        private System.Windows.Forms.Label datiacq;
        private System.Windows.Forms.Label anteprima;
        public System.Windows.Forms.TextBox visdatimonitoraggio;
    }
}