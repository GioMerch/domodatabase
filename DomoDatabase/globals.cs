﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace DomoDatabase
{
    public class globals
    {
        public static domodatabaseacquirenti frmAcquirenti; //= new domodatabaseacquirenti();
        public static domodatabasenodi frmNodi; //= new domodatabasenodi();
        public static domodatabaseperiodi frmPeriodi;// = new domodatabaseperiodi();
        public static domodatabaseresoconti frmResoconti; //= new domodatabaseresoconti();
        public static IncPeriodo frmIncPeriodo;// = new IncPeriodo();
        public static IncNodo frmIncNodo; //= new IncNodo();
        public static IncDati frmIncDati;// = new IncDati();
        public static frmGUI frmIncDati1;
        public static ModbusThread modbusThread = new ModbusThread();

        public static String serverAddress; //localhost //127.0.0.1
        public static String userid;
        public static String password;
        public static String database;
    }
}
