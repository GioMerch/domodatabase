﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Carlo;

namespace DomoDatabase
{
    public partial class IncPeriodo : Form
    {

        public class PeriodoOgg
        {
            public int IDPeriodo, nInst;
            public string start;

            public PeriodoOgg(int id, int n, string s)
            {
                IDPeriodo = id;
                nInst = n;
                start = s;
            }
        }

        public String start = "";
        /*public System.Collections.ArrayList IdPeriodi = new System.Collections.ArrayList();
        public System.Collections.ArrayList starts = new System.Collections.ArrayList();
        public System.Collections.ArrayList numeri_di_installazione = new System.Collections.ArrayList();*/
        public List<PeriodoOgg> periodi = new List<PeriodoOgg>();
        public int ID_periodo = 0;
        public int numero_di_installazione = 0;
        public int ID_nodo = 0;

        public IncPeriodo()
        {
            InitializeComponent();
        }

        public void Comboperiodo()
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT acquirente,numeroinstallazione,inizio,id FROM periodi";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int numero_di_installazione = 0;
                int ID_acquirente = 0;
                String inizio;
                int ID_periodo = 0;
                while (myReader.Read())
                {
                    ID_acquirente = myReader.GetInt32(0);
                    numero_di_installazione = myReader.GetInt32(1);
                    inizio = myReader.GetString(2);
                    ID_periodo = myReader.GetInt32(3);
                    if (ID_acquirente == globals.frmPeriodi.ID)
                    {
                        globals.frmIncPeriodo.periodi.Add(new PeriodoOgg(ID_periodo, numero_di_installazione, inizio));
                        //globals.frmIncPeriodo.starts.Add(inizio);
                        //globals.frmIncPeriodo.numeri_di_installazione.Add(numero_di_installazione);
                        periodoacq.Items.Add(numero_di_installazione + ") " + inizio);
                    }
                }
                myReader.Close();
                myConn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        private void BtnIncPeriodo_Click(object sender, EventArgs e)
        {
            /*foreach (int datonumero in globals.frmIncPeriodo.numeri_di_installazione)
            {
                

                foreach (String datoinizio in globals.frmIncPeriodo.starts)
                {*/
                foreach(PeriodoOgg p in globals.frmIncPeriodo.periodi) { 
                    globals.frmIncPeriodo.numero_di_installazione = p.nInst;
                    globals.frmIncPeriodo.start = p.start;

                    try
                    {
                        if (periodoacq.SelectedItem.Equals(globals.frmIncPeriodo.numero_di_installazione + ") " + globals.frmIncPeriodo.start))
                        {
                            try
                            {
                                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                                string strMiaQuery = "SELECT id,numeroinstallazione,inizio,acquirente FROM periodi";
                                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                                myConn.Open();
                                MySqlDataReader myReader = myCmd.ExecuteReader();
                                int ID_acquirente = 0;
                                int numero_di_installazione = 0;
                                String inizio = "";
                                int ID_periodo = 0;
                                bool i = true;
                                while (i)
                                {
                                    myReader.Read();
                                    ID_periodo = myReader.GetInt32(0);
                                    numero_di_installazione = myReader.GetInt32(1);
                                    inizio = myReader.GetString(2);
                                    ID_acquirente = myReader.GetInt32(3);
                                    if (numero_di_installazione.Equals(globals.frmIncPeriodo.numero_di_installazione) && ID_acquirente == globals.frmPeriodi.ID)
                                    {
                                        i = false;
                                        globals.frmIncPeriodo.ID_periodo = ID_periodo;
                                        globals.frmIncPeriodo.numero_di_installazione = numero_di_installazione;
                                        globals.frmIncPeriodo.start = inizio;
                                    }
                                }
                                myReader.Close();
                                myConn.Close();
                                //IncNodo incnodo = new IncNodo();
                                ////globals.frmIncNodo.Show();
                                //Close();
                                //incnodo.Show();
                                frmGUI incdati = new frmGUI();
                                incdati.Show();
                                Close();
                                globals.frmPeriodi.ctrl10 = true;
                                globals.frmAcquirenti.ctrl2 = true;
                                globals.frmPeriodi.ctrl11 = true;
                                break;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Problemi di connesione al database.");
                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Scegliere il periodo!");
                        break;
                    }
                }
        }

        private void IncPeriodo_Load(object sender, EventArgs e)
        {
            visdatiacq.Text = visdatiacq.Text + "Nome: " + globals.frmAcquirenti.nome + "\tIndirizzo: " + globals.frmAcquirenti.indirizzo + "\tCAP: " + globals.frmAcquirenti.cap + Environment.NewLine + "Città: " + globals.frmAcquirenti.citta + "\tProvincia: " + globals.frmAcquirenti.provincia + "\tLongitudine: " + globals.frmAcquirenti.longitudine + "\tLatitudine: " + globals.frmAcquirenti.latitudine + Environment.NewLine;
            Comboperiodo();
        }

        private void IncPeriodo_FormClosed(object sender, FormClosedEventArgs e)
        {
            globals.frmPeriodi.ctrl10 = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            int num = Int32.Parse(periodoacq.SelectedItem.ToString().Split(')')[0]);
            foreach (PeriodoOgg p in globals.frmIncPeriodo.periodi)
            {
                if (num==p.nInst){
                    dic.Add("periodo", p.IDPeriodo.ToString());
                }
            }
            try
            {
                DictionaryUtility.DictionaryToFile<string, string>(dic, AppDomain.CurrentDomain.BaseDirectory + "config.bin");
                MessageBox.Show("File salvato.");
            }
            catch
            {
                MessageBox.Show("Impossibile salvare il file.");
            }

        }
    }
}
