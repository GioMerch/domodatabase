﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DomoDatabase
{
    public partial class IncDati : Form
    {
        public double temp = 15;
        public double um = 50;
        bool a = false;
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();

        public IncDati()
        {
            InitializeComponent();
        }

        private void eventoTimerMysql()
        {
            a=true;
            myTimer.Stop();
            // Restarts the timer and increments the counter.
            myTimer.Enabled = true;
            DateTime orario = DateTime.Now;
            Random var = new Random();
            int varr = var.Next(0, 10000);
            int varr1 = var.Next(0, 259);
            if (varr%5==1 && globals.frmIncDati.temp != 0 || globals.frmIncDati.temp==30)
            {
                globals.frmIncDati.temp--;
            }
            else
            {
                globals.frmIncDati.temp++;
            }
            if (varr1%5==1 && globals.frmIncDati.um != 0 || globals.frmIncDati.um ==100)
            {
                globals.frmIncDati.um--;
            }
            else
            {
                globals.frmIncDati.um++;
            }
            double A_1 = 0, A_2 = 0, temperatura = globals.frmIncDati.temp, umidita = globals.frmIncDati.um;
            int ID_posizione_nodo = globals.frmIncPeriodo.ID_nodo;
            string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
            string strMiaQuery = "INSERT into dati (ora,temperatura,umidita,nodo,analogico1,analogico2) values (@orario,@temperatura,@umidita,@ID_posizione_nodo,@A_1,@A_2)";
            MySqlConnection myConn = new MySqlConnection(mySelectConn);
            MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
            try
            {
                myConn.Open();
                myCmd.Parameters.AddWithValue("@orario", orario);
                myCmd.Parameters.AddWithValue("@temperatura", temperatura);
                myCmd.Parameters.AddWithValue("@umidita", umidita);
                myCmd.Parameters.AddWithValue("@A_1", A_1);
                myCmd.Parameters.AddWithValue("@A_2", A_2);
                myCmd.Parameters.AddWithValue("@ID_posizione_nodo", ID_posizione_nodo);
                myCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                myConn.Close();
            }
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM dati WHERE ID = (select max(id) from dati)", myConn);
            myConn.Open();
            MySqlDataReader myReader = cmd.ExecuteReader();
            int ID = 0;
            double Analogico_1 = 0;
            double Analogico_2 = 0;
            while (myReader.Read())
            {
                ID = myReader.GetInt32(0);
                orario = myReader.GetDateTime(1);
                temperatura = myReader.GetDouble(2);
                umidita = myReader.GetDouble(3);
                ID_posizione_nodo = myReader.GetInt32(4);
                try
                {
                    Analogico_1 = myReader.GetDouble(5);
                    Analogico_2 = myReader.GetDouble(6);
                }
                catch
                {
                    Analogico_1 = 0;
                    Analogico_2 = 0;
                }
                visincdati.Text = visincdati.Text + "ID: " + ID + "\tOrario: " + orario + "\tTemperatura: " + temperatura + "\tUmidità: " + umidita + "\tAnalogico 1: " + Analogico_1 + "\tAnalogico 2: " + Analogico_2 + "\tID posizione nodo: " + ID_posizione_nodo + Environment.NewLine;
            }
            myReader.Close();
            myConn.Close();
        }

        private void tmrFakeUpdate_Tick(Object sender, EventArgs e)
        {
            eventoTimerMysql();
        }

        private void IncDati_Load(object sender, EventArgs e)
        {
            globals.frmPeriodi.ctrl11 = true;
            globals.frmPeriodi.ctrl10 = true;
            globals.frmAcquirenti.ctrl2 = true;
            visdatinodo.Text = visdatinodo.Text + "ID del nodo: " + globals.frmIncPeriodo.ID_nodo + "\tNumero del nodo: " + globals.frmNodi.numeronodo + "\tPosizione del nodo: " + globals.frmNodi.posizionenodo + "\tNote: " + globals.frmNodi.note + Environment.NewLine;
        }

        private void btnincdati_Click(object sender, EventArgs e)
        {
            if(a==false)
            {
                myTimer.Tick += new EventHandler(tmrFakeUpdate_Tick);
            }
            myTimer.Interval = 2000;
            myTimer.Start();
            Application.DoEvents();
        }

        private void btnstop_Click(object sender, EventArgs e)
        {
            myTimer.Enabled = false;
        }

        private void IncDati_FormClosed(object sender, FormClosedEventArgs e)
        {
            globals.frmPeriodi.ctrl10 = false;
            globals.frmAcquirenti.ctrl2 = true;
            globals.frmPeriodi.ctrl11 = true;
        }

        private void btnindietro1_Click(object sender, EventArgs e)
        {
            IncNodo incnodo = new IncNodo();
            //globals.frmIncNodo.Show();
            Close();
            globals.frmPeriodi.ctrl10 = false;
            globals.frmAcquirenti.ctrl2 = true;
            globals.frmPeriodi.ctrl11 = true;
            incnodo.Show();
        }
    }
}
