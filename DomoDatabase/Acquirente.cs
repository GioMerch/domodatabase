﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DomoDatabase
{
    public partial class domodatabaseacquirenti : Form
    {
        public int ID_acquirente = 0;
        public String nome = "";
        public String indirizzo = "";
        public String citta = "";
        public int cap = 0;
        public String provincia = "";
        public Double longitudine = 0;
        public Double latitudine = 0;
        public int nuovoid = 0;
        public bool ctrl1 = true;
        public bool ctrl2 = true;
        public bool controllo1 = false;

        public domodatabaseacquirenti()
        {
            InitializeComponent();
        }

        private void btnavanti3_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtNome.Text != "" && TxtIndirizzo.Text != "" && TxtCitta.Text != "" && TxtProvincia.Text != "" && TxtCap.Text != "")
                {
                    globals.frmAcquirenti.nome = TxtNome.Text;
                    globals.frmAcquirenti.indirizzo = TxtIndirizzo.Text;
                    globals.frmAcquirenti.citta = TxtCitta.Text;
                    try
                    {
                        globals.frmAcquirenti.cap = Convert.ToInt32(TxtCap.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Inserire CAP corretto!");
                        TxtCap.Text = "";
                        globals.frmAcquirenti.cap = 0;
                    }
                    if(!(globals.frmAcquirenti.cap==0))
                    {
                        if(!(TxtProvincia.Text.Length>2) && !(TxtProvincia.Text.Length<2))
                        {
                            globals.frmAcquirenti.provincia = TxtProvincia.Text;
                            try
                            {
                                globals.frmAcquirenti.longitudine = Convert.ToDouble(TxtLongitudine.Text);
                                globals.frmAcquirenti.latitudine = Convert.ToDouble(TxtLatitudine.Text);
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Longitudine e latitudine non inseriti. Impostati automaticamente a 0.");
                                globals.frmAcquirenti.longitudine = 0;
                                globals.frmAcquirenti.latitudine = 0;
                            }
                            globals.frmAcquirenti.nuovoid = globals.frmPeriodi.ultimoid + 1;
                            globals.frmPeriodi.nomiacq.Items.Add(globals.frmAcquirenti.nuovoid+") " + TxtNome.Text);
                            globals.frmAcquirenti.controllo1 = true;
                            globals.frmPeriodi.apribtn();
                            globals.frmPeriodi.Refresh();
                            globals.frmAcquirenti.ctrl2 = false;
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Inserire la sigla corretta della provincia!");
                            TxtProvincia.Text = "";
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Inserire tutti i dati!");
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Inserire i dati corretti!");
            }
        }

        public void InsAcquirenti()
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "INSERT INTO acquirenti(nome,indirizzo,cap,citta,provincia,longitudine,latitudine) VALUES(@nome,@indirizzo,@cap,@citta,@provincia,@longitudine,@latitudine)";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                myCmd.Parameters.AddWithValue("@nome", globals.frmAcquirenti.nome);
                myCmd.Parameters.AddWithValue("@indirizzo", globals.frmAcquirenti.indirizzo);
                myCmd.Parameters.AddWithValue("@cap", globals.frmAcquirenti.cap);
                myCmd.Parameters.AddWithValue("@citta", globals.frmAcquirenti.citta);
                myCmd.Parameters.AddWithValue("@provincia", globals.frmAcquirenti.provincia);
                try
                {
                    myCmd.Parameters.AddWithValue("@longitudine", globals.frmAcquirenti.longitudine);
                    myCmd.Parameters.AddWithValue("@latitudine", globals.frmAcquirenti.latitudine);
                }
                catch (Exception)
                {
                    myCmd.Parameters.AddWithValue("@longitudine", "0");
                    myCmd.Parameters.AddWithValue("@latitudine", "0");
                }
                finally
                {
                    myCmd.ExecuteNonQuery();
                    myConn.Close();
                }

                mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                strMiaQuery = "SELECT id FROM acquirenti WHERE id = (select max(id) from acquirenti)";
                myConn = new MySqlConnection(mySelectConn);
                myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int ID = 0;
                while (myReader.Read())
                {
                    ID = myReader.GetInt32(0);
                    globals.frmAcquirenti.ID_acquirente = ID;
                    globals.frmAcquirenti.ctrl1 = false;
                }
                myReader.Close();
                myConn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        private void bntannulla_Click(object sender, EventArgs e)
        {
            globals.frmAcquirenti.ctrl2 = false;
            this.Close();
        }

        private void domodatabaseacquirenti_FormClosed(object sender, FormClosedEventArgs e)
        {
            globals.frmAcquirenti.ctrl2 = false;
        }
    }
}
