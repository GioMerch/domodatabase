﻿namespace DomoDatabase
{
    partial class ConfigFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ip = new System.Windows.Forms.TextBox();
            this.nomeU = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.nomeDB = new System.Windows.Forms.TextBox();
            this.creafile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Indirizzo IP server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nome Utente";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nome database";
            // 
            // ip
            // 
            this.ip.Location = new System.Drawing.Point(242, 48);
            this.ip.Name = "ip";
            this.ip.Size = new System.Drawing.Size(232, 22);
            this.ip.TabIndex = 1;
            // 
            // nomeU
            // 
            this.nomeU.Location = new System.Drawing.Point(242, 100);
            this.nomeU.Name = "nomeU";
            this.nomeU.Size = new System.Drawing.Size(232, 22);
            this.nomeU.TabIndex = 2;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(242, 153);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(232, 22);
            this.password.TabIndex = 3;
            // 
            // nomeDB
            // 
            this.nomeDB.Location = new System.Drawing.Point(242, 207);
            this.nomeDB.Name = "nomeDB";
            this.nomeDB.Size = new System.Drawing.Size(232, 22);
            this.nomeDB.TabIndex = 4;
            // 
            // creafile
            // 
            this.creafile.Location = new System.Drawing.Point(336, 253);
            this.creafile.Name = "creafile";
            this.creafile.Size = new System.Drawing.Size(87, 33);
            this.creafile.TabIndex = 5;
            this.creafile.Text = "Crea";
            this.creafile.UseVisualStyleBackColor = true;
            this.creafile.Click += new System.EventHandler(this.creafile_Click);
            // 
            // ConfigFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 317);
            this.Controls.Add(this.creafile);
            this.Controls.Add(this.nomeDB);
            this.Controls.Add(this.password);
            this.Controls.Add(this.nomeU);
            this.Controls.Add(this.ip);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Load += new System.EventHandler(FormLoad);
            this.Name = "ConfigFileForm";
            this.Text = "Configurazione";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ip;
        private System.Windows.Forms.TextBox nomeU;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox nomeDB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button creafile;
    }
}