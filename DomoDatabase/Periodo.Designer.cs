﻿namespace DomoDatabase
{
    partial class domodatabaseperiodi
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnconferma3 = new System.Windows.Forms.Button();
            this.datainizio = new System.Windows.Forms.Label();
            this.btnnuovoacq = new System.Windows.Forms.Button();
            this.nomiacq = new System.Windows.Forms.ComboBox();
            this.selectnomeacq = new System.Windows.Forms.Label();
            this.periodomon = new System.Windows.Forms.Label();
            this.insnuminstall = new System.Windows.Forms.Label();
            this.numinstall = new System.Windows.Forms.TextBox();
            this.calendariodatainizio = new System.Windows.Forms.DateTimePicker();
            this.Incdati = new System.Windows.Forms.Label();
            this.nomiacq1 = new System.Windows.Forms.ComboBox();
            this.BtnIncID = new System.Windows.Forms.Button();
            this.TextIncID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnconferma3
            // 
            this.btnconferma3.Location = new System.Drawing.Point(449, 192);
            this.btnconferma3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnconferma3.Name = "btnconferma3";
            this.btnconferma3.Size = new System.Drawing.Size(100, 28);
            this.btnconferma3.TabIndex = 17;
            this.btnconferma3.Text = "Conferma";
            this.btnconferma3.UseVisualStyleBackColor = true;
            this.btnconferma3.Click += new System.EventHandler(this.btnconferma3_Click);
            // 
            // datainizio
            // 
            this.datainizio.AutoSize = true;
            this.datainizio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datainizio.Location = new System.Drawing.Point(145, 164);
            this.datainizio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.datainizio.Name = "datainizio";
            this.datainizio.Size = new System.Drawing.Size(81, 18);
            this.datainizio.TabIndex = 13;
            this.datainizio.Text = "Data inizio:";
            // 
            // btnnuovoacq
            // 
            this.btnnuovoacq.Location = new System.Drawing.Point(416, 86);
            this.btnnuovoacq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnnuovoacq.Name = "btnnuovoacq";
            this.btnnuovoacq.Size = new System.Drawing.Size(135, 28);
            this.btnnuovoacq.TabIndex = 12;
            this.btnnuovoacq.Text = "Nuovo acquirente";
            this.btnnuovoacq.UseVisualStyleBackColor = true;
            this.btnnuovoacq.Click += new System.EventHandler(this.btnnuovoacq_Click);
            // 
            // nomiacq
            // 
            this.nomiacq.FormattingEnabled = true;
            this.nomiacq.Location = new System.Drawing.Point(245, 89);
            this.nomiacq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nomiacq.Name = "nomiacq";
            this.nomiacq.Size = new System.Drawing.Size(160, 24);
            this.nomiacq.TabIndex = 11;
            this.nomiacq.SelectedIndexChanged += new System.EventHandler(this.nomiacq_SelectedIndexChanged);
            // 
            // selectnomeacq
            // 
            this.selectnomeacq.AutoSize = true;
            this.selectnomeacq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectnomeacq.Location = new System.Drawing.Point(65, 90);
            this.selectnomeacq.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.selectnomeacq.Name = "selectnomeacq";
            this.selectnomeacq.Size = new System.Drawing.Size(150, 18);
            this.selectnomeacq.TabIndex = 10;
            this.selectnomeacq.Text = "Nome dell\'acquirente:";
            // 
            // periodomon
            // 
            this.periodomon.AutoSize = true;
            this.periodomon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodomon.Location = new System.Drawing.Point(107, 30);
            this.periodomon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.periodomon.Name = "periodomon";
            this.periodomon.Size = new System.Drawing.Size(350, 31);
            this.periodomon.TabIndex = 9;
            this.periodomon.Text = "NUOVA INSTALLAZIONE";
            // 
            // insnuminstall
            // 
            this.insnuminstall.AutoSize = true;
            this.insnuminstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insnuminstall.Location = new System.Drawing.Point(49, 127);
            this.insnuminstall.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.insnuminstall.Name = "insnuminstall";
            this.insnuminstall.Size = new System.Drawing.Size(166, 18);
            this.insnuminstall.TabIndex = 18;
            this.insnuminstall.Text = "Numero di Installazione:";
            // 
            // numinstall
            // 
            this.numinstall.Location = new System.Drawing.Point(245, 126);
            this.numinstall.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numinstall.Name = "numinstall";
            this.numinstall.ReadOnly = true;
            this.numinstall.Size = new System.Drawing.Size(39, 22);
            this.numinstall.TabIndex = 19;
            this.numinstall.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // calendariodatainizio
            // 
            this.calendariodatainizio.CustomFormat = "yyyy/MM/dd";
            this.calendariodatainizio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.calendariodatainizio.Location = new System.Drawing.Point(244, 164);
            this.calendariodatainizio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.calendariodatainizio.Name = "calendariodatainizio";
            this.calendariodatainizio.Size = new System.Drawing.Size(160, 22);
            this.calendariodatainizio.TabIndex = 20;
            this.calendariodatainizio.ValueChanged += new System.EventHandler(this.calendariodatainizio_ValueChanged);
            // 
            // Incdati
            // 
            this.Incdati.AutoSize = true;
            this.Incdati.Location = new System.Drawing.Point(57, 281);
            this.Incdati.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Incdati.Name = "Incdati";
            this.Incdati.Size = new System.Drawing.Size(195, 17);
            this.Incdati.TabIndex = 21;
            this.Incdati.Text = "Scegliere ID da incrementare:";
            // 
            // nomiacq1
            // 
            this.nomiacq1.FormattingEnabled = true;
            this.nomiacq1.Location = new System.Drawing.Point(261, 277);
            this.nomiacq1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nomiacq1.Name = "nomiacq1";
            this.nomiacq1.Size = new System.Drawing.Size(160, 24);
            this.nomiacq1.TabIndex = 22;
            // 
            // BtnIncID
            // 
            this.BtnIncID.Location = new System.Drawing.Point(449, 274);
            this.BtnIncID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnIncID.Name = "BtnIncID";
            this.BtnIncID.Size = new System.Drawing.Size(100, 28);
            this.BtnIncID.TabIndex = 23;
            this.BtnIncID.Text = "Avanti";
            this.BtnIncID.UseVisualStyleBackColor = true;
            this.BtnIncID.Click += new System.EventHandler(this.BtnIncID_Click);
            // 
            // TextIncID
            // 
            this.TextIncID.AutoSize = true;
            this.TextIncID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextIncID.Location = new System.Drawing.Point(57, 230);
            this.TextIncID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TextIncID.Name = "TextIncID";
            this.TextIncID.Size = new System.Drawing.Size(308, 24);
            this.TextIncID.TabIndex = 24;
            this.TextIncID.Text = "INCREMENTO DATI SIMULATO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(63, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(319, 24);
            this.label1.TabIndex = 25;
            this.label1.Text = "Creazione file .bin di configuarazione";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(449, 345);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 26;
            this.button1.Text = "Crea";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // domodatabaseperiodi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 389);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextIncID);
            this.Controls.Add(this.BtnIncID);
            this.Controls.Add(this.nomiacq1);
            this.Controls.Add(this.Incdati);
            this.Controls.Add(this.calendariodatainizio);
            this.Controls.Add(this.numinstall);
            this.Controls.Add(this.insnuminstall);
            this.Controls.Add(this.btnconferma3);
            this.Controls.Add(this.datainizio);
            this.Controls.Add(this.btnnuovoacq);
            this.Controls.Add(this.nomiacq);
            this.Controls.Add(this.selectnomeacq);
            this.Controls.Add(this.periodomon);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "domodatabaseperiodi";
            this.Text = "DomoDatabase";
            this.Load += new System.EventHandler(this.domodatabaseperiodi_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.domodatabaseperiodi_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnconferma3;
        private System.Windows.Forms.Label datainizio;
        private System.Windows.Forms.Button btnnuovoacq;
        private System.Windows.Forms.Label selectnomeacq;
        private System.Windows.Forms.Label periodomon;
        public System.Windows.Forms.ComboBox nomiacq;
        private System.Windows.Forms.Label insnuminstall;
        private System.Windows.Forms.TextBox numinstall;
        public System.Windows.Forms.DateTimePicker calendariodatainizio;
        private System.Windows.Forms.Label Incdati;
        public System.Windows.Forms.ComboBox nomiacq1;
        private System.Windows.Forms.Button BtnIncID;
        private System.Windows.Forms.Label TextIncID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}

