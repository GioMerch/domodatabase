﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Carlo;

namespace DomoDatabase
{
    public partial class ConfigFileForm : Form
    {
        public ConfigFileForm()
        {
            InitializeComponent();
        }

        private void FormLoad(object sender, EventArgs e)
        {
            if (File.Exists("configdb.bin"))
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                DictionaryUtility.DictionaryFromFile(AppDomain.CurrentDomain.BaseDirectory + "configdb.bin", out dic);

                ip.Text = dic["serverAddress"];
                nomeU.Text = dic["userid"];
                password.Text = dic["password"];
                nomeDB.Text = dic["database"];
            }
        }

        private void creafile_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("serverAddress", this.ip.Text);
            dic.Add("userid", this.nomeU.Text);
            dic.Add("password", this.password.Text);
            dic.Add("database", this.nomeDB.Text);
            
            try
            {
                DictionaryUtility.DictionaryToFile<string, string>(dic, AppDomain.CurrentDomain.BaseDirectory + "configdb.bin");
                MessageBox.Show("File salvato.");
            }
            catch
            {
                MessageBox.Show("Impossibile salvare il file.");
            }

        }
    }
}
