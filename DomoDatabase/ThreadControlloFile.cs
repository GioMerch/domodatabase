﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DomoDatabase
{
   

    public class ThreadControlloFile
    {
        Thread m_Thread;

        public DateTime t;
        public delegate void CloseDelagate();
        public delegate void RefreshDelegate();
        public bool looping = true;

        public void ControlloFile()
        {
            

            while (looping)
            {
                DateTime tNuovo = File.GetLastWriteTime(AppDomain.CurrentDomain.BaseDirectory + "config.bin");
                if (tNuovo.CompareTo(t) != 0)
                {
                    globals.modbusThread.looping = false;

                    globals.frmIncDati1.Invoke(new CloseDelagate(globals.frmIncDati1.Close));
                    globals.frmIncDati1 = new frmGUI();
                    m_Thread = new Thread(() => System.Windows.Forms.Application.Run(globals.frmIncDati1));
                    m_Thread.Start();

                    looping = false;
                    continue;
                }

                Thread.Sleep(5000);
            }


        }
    }
}
