﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;




/*
======StringUtility
 *   -substring_Da_A(str, int, int) per estrarre una sottostringa senza specificare la lunghezza ma gli indici
 *   -substring-da_a(string string string bool) per estrarre una sottostringa contrsa tra due stringhe. L'ultimo parametro true uindica se le stringhe 
 *                  sul bordo devono essere comprese o meno. Utile per estrarrestringhe tra delimitatori.
 *
 ======DictionaryUtility
 *    -DictionaryToTXTFile          salva un dizionario su un file di testo e lo ricarica. Utile per le opzioni di un programma
 *    -DictionaryFromTXTFile        il dizionario deve essere di sole stringhe
 * 
 *    -DictionaryToFile             come sopra, ma su un file binario utilizzando la serializzazione automatica.
 *    -DictionaryFromFile           è possibile utilizzare dizionari di qualsiasi tipo (stringhe, numeri, classi, eccc)
 *                                  
 *    -AddOrReplace                 su un generico dizionario aggiunge(sostituendo se presente) la coppia specificata nei parametri. Ritorna la presenza o meno della chiave prima dell'inserimento
 * 
*/






















namespace Carlo
{
    //public class debug
    //{
    //    public static void test()
    //    {
    //        DictionaryUtility.testGenericDIC();
    //    }
    //}

    public class myExe
    {
        public static string getSelfPath()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            return (assembly.Location);
        }

        public static string getSelfDir()
        {
            String path = getSelfPath();
            String tmp = System.IO.Path.GetDirectoryName(path);
            return System.IO.Path.GetDirectoryName(path);

        }

        public static System.DateTime RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
            return dt;
        }

        public static string getVersion()
        {
            String versioneDebug = "";
#if DEBUG
            versioneDebug = "DEBUG";
#endif
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(getSelfPath());
            string version = fvi.FileVersion;
            return version + " " + versioneDebug;
        }
    }

    class StringUtility
    {
        public static string substring_Da_A(string str, int da, int a)
        {
            if (da > str.Length)
                return "";
            if (a < 0)
                return "";
            if (a < da)
                return "";

            return (str.Substring(da, a - da + 1));
        }

        public static string substring_Da_A(string str, string da, string a, bool incluse)
        {
            int start = str.IndexOf(da);
            int end = str.IndexOf(a);
            if (incluse)
            {
                end = end + a.Length;
            }
            else
            {
                start = start + da.Length;
            }

            return str.Substring(start, end - start);
        }

        //public static void testReg()
        //{
        //    string aaa = "about cats and dogs";
        //    System.Text.RegularExpressions.Match r = System.Text.RegularExpressions.Regex.Match(aaa, "cat|dog");
        //    MessageBox.Show(r.ToString());
        //    MessageBox.Show(r.NextMatch().ToString());

        //}
    }

    public class DictionaryUtility
    {

        public static void testTXTDictionary()
        {
            Dictionary<string, string> test = new Dictionary<string, string>();

            test.Add("opzione 1", "val 1");
            test.Add("opzione 2", "val 2");
            test.Add("opzione 3", "val 3");
            test.Add("pessimo", "[]{}");
            DictionaryToTXTFile(test, "C:\\dictxt.txt");
        }
        //public static void testGenericDIC()
        //{
        //    Dictionary<int, string> test = new Dictionary<int, string>();
        //    for (int k = 0; k < 10; k++)
        //    {
        //        test.Add(k, ((double)(k + 0.5)).ToString());
        //    }

        //    string val;
        //    test.TryGetValue(5, out val);
        //    MessageBox.Show(val);

        //    DictionaryToFile(test, "C:\\test.bin");

        //    Dictionary<int, string> kkk;
        //    DictionaryFromFile("C:\\test.bin", out kkk);


        //    string val2;
        //    kkk.TryGetValue(5, out val2);
        //    MessageBox.Show(val2);

        //}


        public static bool DictionaryToTXTFile(Dictionary<string, string> d, string path)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {


                //Dictionary<string, string>.Enumerator iter = new Dictionary<string, string>.Enumerator();
                foreach (KeyValuePair<string, string> kvp in d)
                {
                    sw.Write("[");
                    sw.Write(condizionaStringa(kvp.Key.ToString(), true));
                    sw.Write("]{");
                    sw.Write(condizionaStringa(kvp.Value.ToString(), true));
                    sw.WriteLine("}");
                }

            }
            return true;
        }

        public static Dictionary<string, string> DictionaryFromTXTFile(string path)
        {
            Dictionary<string, string> toRet = new Dictionary<string, string>();

            if (System.IO.File.Exists(path))
            {
                List<string[]> parsedData = new List<string[]>();

                try
                {
                    using (StreamReader readFile = new StreamReader(path))
                    {
                        string line;
                        string K;
                        string V;
                        while ((line = readFile.ReadLine()) != null)
                        {
                            K = StringUtility.substring_Da_A(line, "[", "]", false);
                            K = condizionaStringa(K, false);
                            V = StringUtility.substring_Da_A(line, "{", "}", false);
                            V = condizionaStringa(V, false);

                            toRet.Add(K, V);
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Print(e.Message);
                }
            }
            return toRet;
        }

        private static string condizionaStringa(string str) { return condizionaStringa(str, true); }
        private static string deCondizionaStringa(string str) { return condizionaStringa(str, false); }
        private static string condizionaStringa(string str, bool toWrite)
        {
            string ch1A = "[";
            string ch1B = "]";
            string ch2A = "{";
            string ch2B = "}";
            //string replace1A = "<!§specialChar_" + ch1A + "_§!>";
            //string replace1B = "<!§specialChar_" + ch1B + "_§!>";
            //string replace2A = "<!§specialChar_" + ch2A + "_§!>";
            //string replace2B = "<!§specialChar_" + ch2B + "_§!>";
            string replace1A = "<!§specialChar_1a_§!>";
            string replace1B = "<!§specialChar_1b_§!>";
            string replace2A = "<!§specialChar_2a_§!>";
            string replace2B = "<!§specialChar_2b_§!>";



            if (toWrite)
            {
                str = str.Replace(ch1A, replace1A);
                str = str.Replace(ch1B, replace1B);
                str = str.Replace(ch2A, replace2A);
                str = str.Replace(ch2B, replace2B);
            }
            else
            {
                str = str.Replace(replace1A, ch1A);
                str = str.Replace(replace1B, ch1B);
                str = str.Replace(replace2A, ch2A);
                str = str.Replace(replace2B, ch2B);
            }
            return str;
        }

        //private static string myEnc(string str, bool toWrite)
        //{
        //    if (toWrite)
        //    {
        //        str.Replace(@"\", @"\\");
        //        str = str.Replace("[", @"\[");
        //        str = str.Replace("[", @"\]");
        //        str = str.Replace("{", @"\{");
        //        str = str.Replace("}", @"\}");
        //    }
        //    else
        //    {
        //        str = str.Replace(@"\\", @"\");
        //        str = str.Replace(@"\[", "[");
        //        str = str.Replace(@"\]", "]");
        //        str = str.Replace(@"\{", "{");
        //        str = str.Replace(@"\}", "}");
        //    }
        //    return str;
        //}


        public static bool AddOrReplace<Tkey, Tval>(Dictionary<Tkey, Tval> d, Tkey K, Tval V)
        //where Tkey : class//CLASS VA BENE PER STRINGHE E CLASSI, STRUCT PER TIPI STANDARD (ES, DOUBLE);
        //where Tval : class
        {
            bool toRet = false;
            if (d.ContainsKey(K))
            {
                toRet = true;
                d.Remove(K);
            }
            d.Add(K, V);
            return toRet;
        }

        public static bool DictionaryToFile<Tkey, Tval>(Dictionary<Tkey, Tval> d, string path)
        //where Tkey : class//CLASS VA BENE PER STRINGHE E CLASSI, STRUCT PER TIPI STANDARD (ES, DOUBLE);
        //where Tval : class
        {
            String dir = System.IO.Path.GetDirectoryName(path);
            if(dir.Length > 0 &&  !System.IO.Directory.Exists(dir))
                System.IO.Directory.CreateDirectory(dir);

            using (System.IO.FileStream fs = new System.IO.FileStream(path, FileMode.Create))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bf.Serialize(fs, d);
                fs.Close();
            }

            return true;
        }
        public static void DictionaryFromFile<Tkey, Tval>(string path, out Dictionary<Tkey, Tval> toRet)
        //where Tkey : class //CLASS VA BENE PER STRINGHE E CLASSI, STRUCT PER TIPI STANDARD (ES, DOUBLE);
        //where Tval : class
        {
            if (File.Exists(path))
                toRet = new Dictionary<Tkey, Tval>();

            using (System.IO.FileStream fs = new System.IO.FileStream(path, FileMode.Open))
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                toRet = (Dictionary<Tkey, Tval>)bf.Deserialize(fs);
                fs.Close();
            }


        }


    }


    //[Serializable]
    //class mydouble //esempio classe serializzabile
    //{
    //    public double d;
    //    public mydouble(double x)
    //    { d = x; }
    //    public override string ToString()
    //    { return d.ToString(); }
    //}

}

























//public static bool DictionaryToFile(Dictionary<string, string> d, string path)
//{
//    using (System.IO.FileStream fs = new System.IO.FileStream(path, FileMode.Create))
//    {
//        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
//        bf.Serialize(fs, d);
//        fs.Close();
//    }

//    return true;
//}
//public static Dictionary<string, string> DictionaryFromFile(string path)
//{
//    Dictionary<string, string> toRet;
//    if (File.Exists(path))
//        return new Dictionary<string, string>();

//    using (System.IO.FileStream fs = new System.IO.FileStream(path, FileMode.Open))
//    {
//        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
//        toRet = (Dictionary<string, string>)bf.Deserialize(fs);
//        fs.Close();
//        string outstr = "";
//        toRet.TryGetValue("opzione 1", out outstr);
//        MessageBox.Show(outstr);
//    }
//    return toRet;
//}