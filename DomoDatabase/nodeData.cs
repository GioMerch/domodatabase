﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoDatabase
{
    class nodeData
    {
        public int nodeAddress;
        List<dataRecord> readings;

        public nodeData(int nodeAddress)
        {
            this.nodeAddress = nodeAddress;
            readings = new List<dataRecord>();
        }

        public void addMeasure(int temperature, int humidity, int analogic_1, int analogic_2)
        {
            DateTime now = DateTime.Now;
            readings.Add(new dataRecord(now, temperature, humidity, analogic_1, analogic_2));
        }

        public void addMeasure(DateTime timeStamp, int temperature, int humidity, int analogic_1, int analogic_2)
        {
            readings.Add(new dataRecord(timeStamp, temperature, humidity, analogic_1, analogic_2));
        }

    }

    public class dataRecord
    {
        public DateTime timeStamp;
        public double temperature;
        public int humidity;
        public int node;
        public int analogic_1 = 0;
        public int analogic_2 = 0;

        public dataRecord(DateTime timeStamp, double temperature, int humidity, int analogic_1, int analogic_2)                     //Perchè no anche ID Nodo?
        {
            this.timeStamp = timeStamp;
            this.temperature = temperature;
            this.humidity = humidity;
            this.analogic_1 = analogic_1;
            this.analogic_2 = analogic_2;
        }
        public dataRecord(int node, DateTime timeStamp, double temperature, int humidity, int analogic_1, int analogic_2)
        { 
            this.node = node;
            this.timeStamp = timeStamp;
            this.temperature = temperature;
            this.humidity = humidity;
            this.analogic_1 = analogic_1;
            this.analogic_2 = analogic_2;
        }



        public float getDegreesTemp()
        {
            return ((float)temperature / 10.0f);
        }
        public float getHumidityPerc()
        {
            return ((float)humidity / 10.0f);
        }

        public override string ToString()
        {
            String toret = "";
            if (node > 0)
                toret += "nodo: " + node + "\t";
            else
                toret += "nodo non settato";
            toret += "timeStamp: ";
            toret += timeStamp.ToString("HH:mm:ss");
            toret += "\r\n";
            toret += "temperatura: " + getDegreesTemp() + "°C" + "\r\n";
            toret += "umidità: " + getHumidityPerc() + "%" + "\r\n";
            toret += "analogic_1: " + analogic_1 + "°C" + "\r\n";
            toret += "analogic_2: " + analogic_2 + "%" + "\r\n";
            return toret;
        }


    }
}
