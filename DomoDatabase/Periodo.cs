﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using Carlo;

namespace DomoDatabase
{
    public partial class domodatabaseperiodi : Form
    {
        public String start = "";
        public int ID_periodo = 0;
        public int ID = 0;
        public String nome = "";
        public System.Collections.ArrayList nomi = new System.Collections.ArrayList();
        public System.Collections.ArrayList IDs = new System.Collections.ArrayList();
        public int numeroinstall = 0;
        public String id = "";
        public int ultimoid = 0;
        bool inserimentivuoti = true;
        bool inserimentivuoti1 = true;
        bool primoins = true;
        public bool ctrl10 = true;
        public bool ctrl11 = true;

        public void apribtn()
        {
            btnnuovoacq.Enabled = true;
            btnconferma3.Enabled = true;
            BtnIncID.Enabled = true;
            nomiacq.Enabled = true;
            calendariodatainizio.Enabled = true;
            nomiacq1.Enabled = true;
        }

        public void chiudibtn()
        {
            btnnuovoacq.Enabled = false;
            btnconferma3.Enabled = false;
            BtnIncID.Enabled = false;
            nomiacq.Enabled = false;
            calendariodatainizio.Enabled = false;
            nomiacq1.Enabled = false;
        }

        public void datecal()
        {
            String inizio = calendariodatainizio.Value.ToShortDateString();
            DateTime myDate = DateTime.ParseExact(inizio, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            globals.frmPeriodi.start = myDate.ToString("yyyy-MM-dd");
        }

        public void Insperiodo()
        {
            try
            { 
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "INSERT INTO periodi(numeroinstallazione,inizio,acquirente) VALUES(@numeroinstallazione,@inizio,@ID_acquirente)";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                myCmd.Parameters.AddWithValue("@numeroinstallazione", globals.frmPeriodi.numeroinstall);
                myCmd.Parameters.AddWithValue("@inizio", globals.frmPeriodi.start);
                if(globals.frmAcquirenti.ctrl1 == false)
                {
                    myCmd.Parameters.AddWithValue("@ID_acquirente", globals.frmAcquirenti.ID_acquirente);
                }
                else
                {
                    myCmd.Parameters.AddWithValue("@ID_acquirente", globals.frmPeriodi.ID);
                }
                myCmd.ExecuteNonQuery();
                myConn.Close();
                mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                strMiaQuery = "SELECT id FROM periodi WHERE id = (select max(id) from periodi)";
                myConn = new MySqlConnection(mySelectConn);
                myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int ID = 0;
                while (myReader.Read())
                {
                    ID = myReader.GetInt32(0);
                    globals.frmPeriodi.ID_periodo = ID;
                }
                myReader.Close();
                myConn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        public void Visdatiacq()
        {
            foreach (int datoid in globals.frmPeriodi.IDs)
            {
                globals.frmPeriodi.ID = datoid;

                foreach (String datonome in globals.frmPeriodi.nomi)
                {
                    globals.frmPeriodi.nome = datonome;
                    if (nomiacq.SelectedItem.Equals(globals.frmPeriodi.ID + ") " + globals.frmPeriodi.nome))
                    {
                        try
                        { 
                            string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                            string strMiaQuery = "SELECT * FROM acquirenti";
                            MySqlConnection myConn = new MySqlConnection(mySelectConn);
                            MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                            myConn.Open();
                            MySqlDataReader myReader = myCmd.ExecuteReader();
                            int _ID = 0;
                            string Nome = "";
                            string Indirizzo = "";
                            int CAP = 0;
                            string Citta = "";
                            string Provincia = "";
                            double Posizione_GPS_lat;
                            double Posizione_GPS_lng;
                            bool i = true;
                            while (i)
                            {
                                myReader.Read();
                                _ID = myReader.GetInt32(0);
                                Nome = myReader.GetString(1);
                                Indirizzo = myReader.GetString(2);
                                CAP = myReader.GetInt32(3);
                                Citta = myReader.GetString(4);
                                Provincia = myReader.GetString(5);
                                try
                                {
                                    Posizione_GPS_lng = myReader.GetDouble(6);
                                    Posizione_GPS_lat = myReader.GetDouble(7);
                                }
                                catch
                                {
                                    Posizione_GPS_lng = 0;
                                    Posizione_GPS_lat = 0;
                                }
                                if (_ID.Equals(globals.frmPeriodi.ID))
                                {
                                    i = false;
                                    globals.frmAcquirenti.nome = Nome;
                                    globals.frmAcquirenti.indirizzo = Indirizzo;
                                    globals.frmAcquirenti.citta = Citta;
                                    globals.frmAcquirenti.cap = CAP;
                                    globals.frmAcquirenti.provincia = Provincia;
                                    globals.frmAcquirenti.longitudine = Posizione_GPS_lng;
                                    globals.frmAcquirenti.latitudine = Posizione_GPS_lat;
                                }
                            }
                            myReader.Close();
                            myConn.Close();
                            break;
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Problemi di connesione al database.");
                        }
                    }
                }
                if(nomiacq.SelectedItem.Equals(globals.frmPeriodi.ID + ") " + globals.frmPeriodi.nome))
                {
                    break;
                }
            }
        }

        public void Visnumistall()
        {
            try
            {
                globals.frmPeriodi.numeroinstall = 1;
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT acquirente,numeroinstallazione FROM periodi";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                int numero_di_installazione = 0;
                int ID_acquirente = 0;
                while (myReader.Read())
                {
                    ID_acquirente = myReader.GetInt32(0);
                    numero_di_installazione = myReader.GetInt32(1);
                    if (ID_acquirente == Convert.ToInt32(globals.frmPeriodi.id))
                    {
                        numinstall.Text = "";
                        globals.frmPeriodi.numeroinstall++;
                        numinstall.Text = numinstall.Text + globals.frmPeriodi.numeroinstall;
                    }
                    else
                    {
                        if (globals.frmPeriodi.numeroinstall < 2)
                        {
                            numinstall.Text = "";
                            globals.frmPeriodi.numeroinstall = 1;
                            numinstall.Text = numinstall.Text + globals.frmPeriodi.numeroinstall;
                        }
                    }
                }
                myReader.Close();
                myConn.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        public void ClienteCombo()
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT ID,nome FROM acquirenti";
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                globals.frmPeriodi.ultimoid = 0;
                while (myReader.Read())
                {
                    int tID = myReader.GetInt32(0);
                    String tnome = myReader.GetString(1);
                    globals.frmPeriodi.nomi.Add(tnome);
                    globals.frmPeriodi.IDs.Add(tID);
                    globals.frmPeriodi.ultimoid = myReader.GetInt32(0);
                    nomiacq.Items.Add(tID + ") " + tnome);
                    nomiacq1.Items.Add(tID + ") " + tnome);
                }
                myReader.Close();
                myConn.Close();
            }
            catch(Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        /// <summary>
        /// carica la configurazione del programma
        /// </summary>
        public void loadConfig()
        {
            try
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                DictionaryUtility.DictionaryFromFile(AppDomain.CurrentDomain.BaseDirectory + "configdb.bin", out dic);
                globals.serverAddress = dic["serverAddress"];
                globals.userid = dic["userid"];
                globals.password = dic["password"];
                globals.database = dic["database"];
            }
            catch
            {
                MessageBox.Show("File contenente le credenziali non trovato.");
            }

            /*
            String kkkkk = "asdasdsa=2ddewdweq";

            char[] separatori = new char[] { '=', ';', '?' };
            String[] pezz = kkkkk.Split(separatori);
            kkkkk.Trim();
            kkkkk.Contains("=");

            //se il file esiste
            {

                ///leggere da file la configurazione


                //scrivere nelle var i valori

                //globals.serverAddress = ....
            }
            */

        }

        public domodatabaseperiodi()
        {
            globals.frmPeriodi = this;
            globals.frmAcquirenti = new domodatabaseacquirenti();
            globals.frmNodi = new domodatabasenodi();
            globals.frmResoconti = new domodatabaseresoconti();
            globals.frmIncPeriodo = new IncPeriodo();
            globals.frmIncNodo = new IncNodo();
            globals.frmIncDati = new IncDati();
            globals.frmIncDati1 = new frmGUI();
            InitializeComponent();
            loadConfig();
        }

        private void btnnuovoacq_Click(object sender, EventArgs e)
        {
            chiudibtn();
            domodatabaseacquirenti frmacquirenti = new domodatabaseacquirenti();
            globals.frmAcquirenti.ctrl2 = true;
            globals.frmPeriodi.ctrl11 = true;
            globals.frmPeriodi.ctrl10 = true;
            frmacquirenti.Show();
        }

        public void domodatabaseperiodi_Load(object sender, EventArgs e)
        {
            ClienteCombo();
        }

        private void btnconferma3_Click(object sender, EventArgs e)
        {
            if(inserimentivuoti1==false && inserimentivuoti == false)
            {
                Visdatiacq();
                globals.frmNodi.numeronodi.Clear();
                globals.frmNodi.posizionenodi.Clear();
                globals.frmNodi.notes.Clear();
                globals.frmNodi.datinodi.Clear();
                chiudibtn();
                domodatabasenodi frmnodi = new domodatabasenodi();
                frmnodi.Show();
                globals.frmPeriodi.ctrl11 = true;
                globals.frmPeriodi.ctrl10 = true;
                globals.frmAcquirenti.ctrl2 = true;
            }
            else
            {
                MessageBox.Show("Inserire tutti i dati!");
            }
        }

        private void nomiacq_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int contrl in globals.frmPeriodi.IDs)
            {
                primoins = false;
                String selezionato = Convert.ToString(nomiacq.SelectedItem);
                String[] pezzi = selezionato.Split(')');
                globals.frmPeriodi.id = pezzi[0];
                if(!(id==Convert.ToString(globals.frmAcquirenti.nuovoid)))
                {
                    Visnumistall();
                }
                else
                {
                    globals.frmPeriodi.ID = 0;
                    globals.frmPeriodi.numeroinstall = 1;
                    numinstall.Text = "";
                    numinstall.Text = numinstall.Text + globals.frmPeriodi.numeroinstall;
                }
                break;
            }
            if(primoins==true)
            {
                globals.frmPeriodi.ID = 0;
                globals.frmPeriodi.numeroinstall = 1;
                numinstall.Text = "";
                numinstall.Text = numinstall.Text + globals.frmPeriodi.numeroinstall;
            }
            inserimentivuoti = false;
        }

        private void calendariodatainizio_ValueChanged(object sender, EventArgs e)
        {
            datecal();
            inserimentivuoti1 = false;
        }

        public void BtnIncID_Click(object sender, EventArgs e)
        {
            foreach (int datoid in globals.frmPeriodi.IDs)
            {
                globals.frmPeriodi.ID = datoid;

                foreach (String datonome in globals.frmPeriodi.nomi)
                {
                    globals.frmPeriodi.nome = datonome;
                    try
                    {
                        if (nomiacq1.SelectedItem.Equals(globals.frmPeriodi.ID + ") " + globals.frmPeriodi.nome))
                        {
                            try
                            {
                                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                                string strMiaQuery = "SELECT * FROM acquirenti";
                                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                                myConn.Open();
                                MySqlDataReader myReader = myCmd.ExecuteReader();
                                int _ID = 0;
                                string Nome = "";
                                string Indirizzo = "";
                                int CAP = 0;
                                string Citta = "";
                                string Provincia = "";
                                double Posizione_GPS_lat;
                                double Posizione_GPS_lng;
                                bool i = true;
                                while (i)
                                {
                                    myReader.Read();
                                    _ID = myReader.GetInt32(0);
                                    Nome = myReader.GetString(1);
                                    Indirizzo = myReader.GetString(2);
                                    CAP = myReader.GetInt32(3);
                                    Citta = myReader.GetString(4);
                                    Provincia = myReader.GetString(5);
                                    try
                                    {
                                        Posizione_GPS_lng = myReader.GetDouble(6);
                                        Posizione_GPS_lat = myReader.GetDouble(7);
                                    }
                                    catch
                                    {
                                        Posizione_GPS_lng = 0;
                                        Posizione_GPS_lat = 0;
                                    }
                                    if (_ID.Equals(globals.frmPeriodi.ID))
                                    {
                                        i = false;
                                        globals.frmAcquirenti.nome = Nome;
                                        globals.frmAcquirenti.indirizzo = Indirizzo;
                                        globals.frmAcquirenti.citta = Citta;
                                        globals.frmAcquirenti.cap = CAP;
                                        globals.frmAcquirenti.provincia = Provincia;
                                        globals.frmAcquirenti.longitudine = Posizione_GPS_lng;
                                        globals.frmAcquirenti.latitudine = Posizione_GPS_lat;
                                    }
                                }
                                myReader.Close();
                                myConn.Close();
                                chiudibtn();
                                globals.frmPeriodi.ctrl11 = true;
                                globals.frmAcquirenti.ctrl2 = true;
                                globals.frmPeriodi.ctrl10 = true;
                                IncPeriodo incperiodo = new IncPeriodo();
                                incperiodo.Show();
                                break;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Problemi di connesione al database.");
                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Scegliere l'ID!");
                        break;
                    }
                }
                try
                {
                    if (nomiacq1.SelectedItem.Equals(globals.frmPeriodi.ID + ") " + globals.frmPeriodi.nome))
                    {
                        break;
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        private void domodatabaseperiodi_MouseClick(object sender, MouseEventArgs e)
        {
            if (globals.frmAcquirenti.ctrl2 == false || globals.frmPeriodi.ctrl11 == false || globals.frmPeriodi.ctrl10 == false)
            {
                globals.frmPeriodi.apribtn();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigFileForm f = new ConfigFileForm();
            f.Show();
        }
    }
}
