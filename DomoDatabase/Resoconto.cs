﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DomoDatabase
{
    public partial class domodatabaseresoconti : Form
    {
        int i = 0;
        public int n = 1;
        public bool controlloalproxform = false;

        public domodatabaseresoconti()
        {
            InitializeComponent();
        }

        private void btnconferma_Click(object sender, EventArgs e)
        {
            if(globals.frmAcquirenti.controllo1==true)
            {
                globals.frmAcquirenti.InsAcquirenti();
            }
            globals.frmPeriodi.Insperiodo();
            globals.frmNodi.Insnodi();
            Visnumnodi();
            frmGUI frmgui = new frmGUI();
            frmgui.Show();
            Close();
        }

        private void domodatabaseresoconti_Load(object sender, EventArgs e)
        {
            globals.frmPeriodi.ctrl11 = true;
            globals.frmPeriodi.ctrl10 = true;
            globals.frmAcquirenti.ctrl2 = true;
            visdatiacq.Text = visdatiacq.Text + "Nome: " + globals.frmAcquirenti.nome + "\tIndirizzo: " + globals.frmAcquirenti.indirizzo + "\tCAP: " + globals.frmAcquirenti.cap + Environment.NewLine + "Città: " + globals.frmAcquirenti.citta + "\tProvincia: " + globals.frmAcquirenti.provincia + "\tLongitudine: " + globals.frmAcquirenti.longitudine + "\tLatitudine: " + globals.frmAcquirenti.latitudine + Environment.NewLine;
            visdatimonitoraggio.Text = visdatimonitoraggio.Text + "Data di inizio: " + globals.frmPeriodi.start + "\tNumero di Installazione: "+globals.frmPeriodi.numeroinstall+"°";
            foreach(String _datonodo in globals.frmNodi.datinodi)
            {
                globals.frmNodi.noditot = _datonodo;
                i++;
                foreach (int _numeronodo in globals.frmNodi.numeronodi)
                {
                    globals.frmNodi.numeronodo = _numeronodo;
                    foreach (String _posizionenodo in globals.frmNodi.posizionenodi)
                    {
                        globals.frmNodi.posizionenodo = _posizionenodo;
                        foreach (String _note in globals.frmNodi.notes)
                        {
                            globals.frmNodi.note = _note;
                            if ((globals.frmNodi.numeronodo + globals.frmNodi.posizionenodo + globals.frmNodi.note) == globals.frmNodi.noditot && i == globals.frmResoconti.n)
                            {
                                globals.frmResoconti.n++;
                                visdatinodi.Text = visdatinodi.Text + "N°: " + globals.frmNodi.numeronodo + "\tStanza: " + globals.frmNodi.posizionenodo + "\tNote: " + globals.frmNodi.note + Environment.NewLine;
                            }
                        }
                    }
                }
            }
        }

        private void domodatabaseresoconti_FormClosed(object sender, FormClosedEventArgs e)
        {
            //globals.frmPeriodi.ctrl11 = false;
        }

        public void Visnumnodi()
        {
            try
            {
                string mySelectConn = @"server=" + globals.serverAddress + ";userid=" + globals.userid + ";password=" + globals.password + ";database=" + globals.database;
                string strMiaQuery = "SELECT id, indirizzo, posizione FROM nodi WHERE periodo=" + globals.frmPeriodi.ID_periodo;
                MySqlConnection myConn = new MySqlConnection(mySelectConn);
                MySqlCommand myCmd = new MySqlCommand(strMiaQuery, myConn);
                myConn.Open();
                MySqlDataReader myReader = myCmd.ExecuteReader();
                globals.frmIncDati1.IDnodi.Clear();
                int ID = 0, numeronodo = 0;
                String posizionenodo = "";
                while (myReader.Read())
                {
                    ID = myReader.GetInt32(0);
                    numeronodo = myReader.GetInt32(1);
                    posizionenodo = myReader.GetString(2);

                    IncNodo nodo = new IncNodo();
                    nodo.ID_nodo = ID;
                    nodo.numeronodo = numeronodo;
                    nodo.posizionenodo = posizionenodo;
                    globals.frmIncDati1.IDnodi.Add(nodo);

                }
                globals.frmResoconti.controlloalproxform = true;
                myReader.Close();
                myConn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problemi di connesione al database.");
            }
        }

        private void domodatabaseresoconti_FormClosing(object sender, FormClosingEventArgs e)
        {
            globals.frmPeriodi.apribtn();
        }
    }
}
