﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace DomoDatabase
{
    public class ModbusThread
    {
        public List<dataRecord> ultimaLettura;
        public List<string> stringhe;

        public TAGLaser.ModBus.modbusSerial modbusSerial;

        public bool looping = true;
        public bool idle = false;

        public frmGUI.delegAddLog caronte;

        public List<int> connectedNodes = new List<int>();

        public void loop()
        {
            ultimaLettura = new List<dataRecord>();
            //stringhe = new List<string>();

            while (looping)
            {
                if (idle)
                {
                    System.Threading.Thread.Sleep(50);
                    //System.Console.Write(".");
                }
                else
                {
                    ultimaLettura.Clear();
                    
                    for (int k = 0; k < connectedNodes.Count; k++)
                    {
                        DateTime now = DateTime.Now;

#if DEBUG || MODBUS
                        double temp = interrogaTempFake(connectedNodes[k]);
                        int hum = interrogaHumFake(connectedNodes[k]);
                        int A_1 = 0;
                        int A_2 = 0;
                        dataRecord lastRecord = new dataRecord(connectedNodes[k], now, temp, hum,A_1,A_2);
                        ultimaLettura.Add(lastRecord);
                        System.Console.Write("*");

#else
                        short[] registri = leggiRegistri(connectedNodes[k]);
                        if (registri == null)
                        {
                            continue;
                        }
                        dataRecord lastRecord = new dataRecord(connectedNodes[k], now, registri[0], registri[1], registri[2], registri[3]);
                        ultimaLettura.Add(lastRecord);
#endif
                    }
                    caronte();
                    Thread.Sleep(1000);
                }
            }
        }


        double fakeTemp = 260;
        public double interrogaTempFake(int nodo)
        {
            Random r = new Random();
            Thread.Sleep(300);
            fakeTemp = fakeTemp + r.Next(-3, 3);
            return fakeTemp;
        }
        int fakeHum = 80;
        public int interrogaHumFake(int nodo)
        {
            Thread.Sleep(300);
            return 80;
        }

        

        public short[] leggiRegistri(int nodo)
        {
            ushort start = (ushort)(((nodo - 1) * 10) + 6);

            short[] regs = modbusSerial.LeggiValori(1, start, 4);
            
            return regs;
        }

    }

}
