﻿namespace DomoDatabase
{
    partial class IncPeriodo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Selectperiodoinc = new System.Windows.Forms.Label();
            this.periodoacq = new System.Windows.Forms.ComboBox();
            this.BtnIncPeriodo = new System.Windows.Forms.Button();
            this.visdatiacq = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Selectperiodoinc
            // 
            this.Selectperiodoinc.AutoSize = true;
            this.Selectperiodoinc.Location = new System.Drawing.Point(8, 96);
            this.Selectperiodoinc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Selectperiodoinc.Name = "Selectperiodoinc";
            this.Selectperiodoinc.Size = new System.Drawing.Size(243, 17);
            this.Selectperiodoinc.TabIndex = 0;
            this.Selectperiodoinc.Text = "Seleziona il periodo da incrementare:";
            // 
            // periodoacq
            // 
            this.periodoacq.FormattingEnabled = true;
            this.periodoacq.Location = new System.Drawing.Point(256, 92);
            this.periodoacq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.periodoacq.Name = "periodoacq";
            this.periodoacq.Size = new System.Drawing.Size(160, 24);
            this.periodoacq.TabIndex = 23;
            // 
            // BtnIncPeriodo
            // 
            this.BtnIncPeriodo.Location = new System.Drawing.Point(452, 90);
            this.BtnIncPeriodo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnIncPeriodo.Name = "BtnIncPeriodo";
            this.BtnIncPeriodo.Size = new System.Drawing.Size(100, 28);
            this.BtnIncPeriodo.TabIndex = 24;
            this.BtnIncPeriodo.Text = "Avanti";
            this.BtnIncPeriodo.UseVisualStyleBackColor = true;
            this.BtnIncPeriodo.Click += new System.EventHandler(this.BtnIncPeriodo_Click);
            // 
            // visdatiacq
            // 
            this.visdatiacq.Location = new System.Drawing.Point(17, 16);
            this.visdatiacq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.visdatiacq.Multiline = true;
            this.visdatiacq.Name = "visdatiacq";
            this.visdatiacq.ReadOnly = true;
            this.visdatiacq.Size = new System.Drawing.Size(533, 52);
            this.visdatiacq.TabIndex = 25;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(452, 125);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 32);
            this.button1.TabIndex = 26;
            this.button1.Text = "Salva .bin";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IncPeriodo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 169);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.visdatiacq);
            this.Controls.Add(this.BtnIncPeriodo);
            this.Controls.Add(this.periodoacq);
            this.Controls.Add(this.Selectperiodoinc);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IncPeriodo";
            this.Text = "IncPeriodo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IncPeriodo_FormClosed);
            this.Load += new System.EventHandler(this.IncPeriodo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Selectperiodoinc;
        public System.Windows.Forms.ComboBox periodoacq;
        private System.Windows.Forms.Button BtnIncPeriodo;
        private System.Windows.Forms.TextBox visdatiacq;
        private System.Windows.Forms.Button button1;
    }
}